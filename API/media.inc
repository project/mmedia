<?php

/**
 * Load a media item.
 *
 * @param $mid
 *   The media identifier from the database.
 * @param $refresh
 *   Refreshes the memory list.
 * @return
 *   Returns false on failure, media object on success.
 */
function media_load($mid, $refresh = FALSE) {
  static $media;

  // refresh the cache
  if (!isset($media) || (empty($mid) && $refresh)) {
    $media = array();
  }

  if (!is_numeric($mid)) {
    return FALSE;
  }

  // if media cached
  if (array_key_exists($mid, $media) && !$refresh) {
    return $media[$mid];
  }

  // load from database
  $m = db_fetch_object(db_query("SELECT * FROM {media} WHERE mid = %d", $mid));

  // not a valid item
  if (!$m) {
    return $m;
  }

  // unserialize stored data
  $m->dimensions = unserialize($m->dimensions);

  // notify others that the media is deleted
  media_invoke_item('load', $m);

  // save into cache
  $media[$mid] = $m;

  // return object
  return $media[$mid];
}

/**
 * Insert a media item.
 *
 * @param $media
 *   The media upload object
 * @param $fid
 *   The folder id for the media object.
 * @return
 *   Returns false on failure, media object on success.
 */
function media_insert($media, $fid = 0, $workspace = 'mmedia') {
  // creates a media item from filename
  global $user;

  // check that some basics are actually set
  if (!is_object($media) || !(@$media->title) || (!mapi_file_exists(@$media->path) && !mapi_file_exists(@$media->path . @$media->name .'.'. @$media->ext))) {
    return FALSE;
  }

  // load parent to check
  if (!($parent = mmedia_folder_load($fid))) {
    return FALSE;
  }

  // find a unique hash for the media.
  $count = 1;
  $test = serialize($media);
  do {
    // generate hash, hopefully unique given all the parts
    $hash = md5($test . $count++);
    $result = db_result(db_query("SELECT hash FROM {media} WHERE hash='%s'", $hash));
  } while ($result);

  // determine path
  $path = media_invoke_item('path', $media);
  // default to public directory with path
  if (empty($path)) {
    $path = media_directory_public() . $parent->path;
  }
  // otherwise take the first path off the array
  else {
    $path = array_shift($path);
  }

  // make sure it exists, within workspace
  if (!mapi_directory_check($path, array('create' => TRUE, 'workspace' => $workspace))) {
    mapi_error('Unable to create directory !path', array('!path' => $path), 'media');
    return FALSE;
  }

  // get the file details
  $name = (!empty($media->name) ? $media->name : mapi_file_name($media->path));
  $ext  = (!empty($media->ext)  ? $media->ext : mapi_file_ext($media->path));
  $fname = $name .'.'. $ext;

  // if path is the only thing set, then change $media->path accordingly
  if (empty($media->name)) {
    $media->path = dirname($media->path) .'/';
  }

  // check status of external website
  $external = !mapi_file_is_local($media->path . $fname);

  // copy the file, but only if local
  if (!$external) {
    if (!($file = mapi_file_copy($media->path . $fname, $path . $fname, array('workspace' => $workspace)))) {
      mapi_error('Unable to copy !filename to !path', array('!filename' => $media->path . $fname, '!path' => $path . $fname));
      return FALSE;
    }
  }
  // it's an external file
  else {
    $file = $media->path . $fname;
  }

  // check for properly filled out values
  $media->path        = mapi_file_path($file);
  $media->name        = mapi_file_name($file);
  $media->ext         = mapi_file_ext($file);
  $media->title       = (!empty($media->title) ? $media->title : t('Untitled'));
  $media->mime        = (!empty($media->mime) ? $media->mime : mapi_extension_mime($media->ext));
  $media->dimensions  = (!empty($media->dimensions) ? $media->dimensions : mapi_extension_dimensions($file));
  $media->type        = (!empty($media->type) ? $media->type : mapi_extension_type($media->ext));
  $media->created     = (!empty($media->created) ? $media->created : time());
  $media->changed     = time();
  $media->expire      = (!empty($media->expire) ? $media->expire : 0);
  $media->local       = $external;
  $media->public      = (!empty($media->public) ? $media->public : TRUE);
  $media->summary     = (!empty($media->summary) ? drupal_substr($media->summary, 0, 255) : '');
  $media->source      = (!empty($media->source) ? drupal_substr($media->source, 0, 255) : '');
  $media->reference   = (!empty($media->reference) ? drupal_substr($media->reference, 0, 255): '');

  // define the media and it's values to insert into the database
  $mid = @$media->mid;
  $media_objects = array('mid' => $mid, 'fid' => $parent->fid, 'uid' => $user->uid,
    'title' => $media->title, 'name' => $media->name, 'mime' => $media->mime, 'hash' => $hash,
    'dimensions' => serialize($media->dimensions), 'type' => $media->type, 'ext' => $media->ext,
    'created' => $media->created, 'changed' => $media->changed, 'path' => $media->path,
    'summary' => $media->summary, 'source' => $media->source, 'reference' => $media->reference,
    'expire' => $media->expire, 'local' => $media->local, 'public' => $media->public,
  );
  $media_values = array('mid' => '%d', 'fid' => '%d', 'uid' => '%d',
    'title' => "'%s'", 'name' => "'%s'", 'mime' => "'%s'", 'hash' => "'%s'",
    'dimensions' => "'%s'", 'type' => "'%s'", 'ext' => "'%s'",
    'created' => '%d', 'changed' => '%d', 'path' => "'%s'",
    'summary' => "'%s'", 'source' => "'%s'", 'reference' => "'%s'",
    'expire' => '%d', 'local' => '%d', 'public' => '%d',
  );

  // add this folder to the database
  if (db_query('INSERT INTO {media} ('. implode(', ', array_keys($media_objects)) .') VALUES ('. implode(', ', $media_values) .')', $media_objects)) {
    $mid = db_last_insert_id('media', 'mid');
    $media->mid = $mid;
    watchdog('media', 'Created media :(!mid) !dir', array('!mid' => $mid, '!dir' => $media->path . $media->name .'.'. $media->ext));
  }
  // otherwise return false with error
  else {
    mapi_error('Unable to insert media into database:'. $file);
    return FALSE;
  }

  // notify others that the media as been created
  media_invoke_item('create', $media);

  // reload the item, thereby giving the correct details from the database
  $media = media_load($mid);

  // return the newly created media
  return $media;
}

/**
 * Create a media item from a filename, or a URL.
 *
 * @param $filename
 *   The filename to copy.
 * @param $fid
 *   The folder identifier for the media item.
 * @return
 *   Returns false on failure, media object on success.
 */
function media_create($uri, $title, $parent = 0, $workspace = 'mmedia') {
  // must have title and a filename that exists
  if (!$title || !mapi_file_exists($uri)) {
    return FALSE;
  }

  // create an object which can be processed correctly
  $media = new stdClass;
  $media->path = $uri;
  $media->title = $title;

  return media_insert($media, $parent, $workspace);
}

/**
 * Delete an item from the database.
 *
 * @param $mid
 *   The identifier for the media item.
 */
function media_delete($mid, $workspace = NULL) {
  // load item from database
  if (!($media = media_load($mid))) {
    return FALSE;
  }

  $path = $media->path . $media->name .'.'. $media->ext;
  // delete the original file if local
  if ($media->local) {
    if (!mapi_file_delete($path, $workspace)) {
      mapi_error('Failed to delete media original (%file).', array('%file' => $media->path . $media->name .'.'. $media->ext));
    }
  }

  // makes sure the generated derivatives for this media are removed
  if (module_exists('mapi_derivatives')) {
    mapi_generated_flush($path);
  }

  // remove the media
  db_query("DELETE FROM {media} WHERE mid=%d", $media->mid);

  // remove common metadata
  db_query("DELETE FROM {media_extras} WHERE mid=%d", $media->mid);

  // remove the extra details that are associated with the media
  db_query("DELETE FROM {media_metadata} WHERE mid=%d", $media->mid);

  // notify others that the media is deleted
  media_invoke_item('delete', $media);
}

/**
 * Rename an item, including original and derivatives.
 *
 * @param $mid
 *   The identifier for the media item.
 * @param $name
 *   The new name, invalid characters filtered.
 * @return
 *   Return FALSE on failure, TRUE on success
 */
function media_rename($mid, $name, $workspace = 'mmedia') {
  // load derivative
  if (!($media = media_load($mid))) {
    return FALSE;
  }

  // cannot rename a non-local file
  if (!$media->local) {
    return TRUE;
  }

  // replace potential invalid characters with '-'
  $name = preg_replace('/[^\w\d \-_\(\)\[\]\"\']+/i', '-', $name);

  // update the media settings
  $old_media = drupal_clone($media);
  $media->name = $name;

  $source = media_filename($old_media);
  $dest = media_filename($media);

  // rename the file on disk
  if ($file = mapi_file_move($source, $dest, FILE_EXISTS_RENAME, $workspace)) {
    $media->name = mapi_file_basename($file);
  }
  else {
    mapi_error('Unable to rename file (%source) to (%dest).', array('%source' => $source, '%dest' => $dest));
    return FALSE;
  }

  db_query("UPDATE {media} SET name='%s' WHERE mid=%d", $media->name, $media->mid);

  media_load($media->mid, TRUE);

  // notify others that the media has been renamed
  $media->old_name = $old_media->name;
  media_invoke_item('rename', $media);

  return TRUE;
}

/**
 * Move an item from one folder to another. This moves the derivatives if the media system is
 * set to a hierarchical structure.
 *
 * @param $mid
 *   The identifier for the media item.
 * @param $fid
 *   The new folder to move media item to.
 * @return
 *   Return FALSE on failure, TRUE on success
 */
function media_move($mid, $fid, $workspace = 'mmedia') {
  // load the media and folder
  if (!($media = media_load($mid)) || !($folder = mmedia_folder_load($fid))) {
    return FALSE;
  }

  // no real need to shift it (if folder is the same, or non-local)
  if ($media->fid == $folder->fid || !$media->local) {
    return TRUE;
  }

  // update the media settings
  $old_media = drupal_clone($media);
  $media->fid = $folder->fid;

  // rename the media itself
  $source = $media->path . $media->name .'.'. $media->ext;

  // change the folder path
  $media->path = media_folder($media, $workspace, TRUE);
  $dest = media_filename($media);

  if ($file = mapi_file_move($source, $dest, FILE_EXISTS_RENAME, $workspace)) {
    watchdog('media', 'Moved file from (%source) to (%dest)', array('%source' => media_directory_public() . $source, '%dest' => media_directory_public() . $dest));
    db_query("UPDATE {media} SET name='%s', path='%s' WHERE mid=%d", mapi_file_name($file), $media->path, $media->mid);
  }
  else {
    mapi_error('Unable to rename file (%source) to (%dest)', array('%source' => $source, '%dest' => $dest));
    return FALSE;
  }

  // change the media folder
  db_query("UPDATE {media} SET fid=%d, path='%s' WHERE mid=%d", $fid, $media->path, $media->mid);

  media_load($media->mid, TRUE);

  $media->old_ext = $old_media->ext;
  $media->old_name = $old_media->name;
  $media->old_path = $old_media->path;
  $media->old_filename = media_filename($old_media);
  $media->old_fid = $old_media->fid;

  // notify others that the media has been moved
  media_invoke_item('move', $media);

  return TRUE;
}

/**
 * Load the metadata for the item.
 *
 * @param $mid
 *   The media identifier.
 * @return
 *   Returns an associative key array for the item.
 */
function media_metadata_load($mid, $refresh = FALSE) {
  static $media;

  $metadata = array();
  if (!isset($media) || (empty($mid) && $refresh)) {
    $media = array();
  }
  if (array_key_exists($mid, $media) && !$refresh) {
    return $media[$mid];
  }

  // load inbuilt metadata
  $inbuilt = array('title', 'summary', 'source', 'reference', 'created', 'expire');
  $m = media_load($mid);
  foreach ($inbuilt as $key) {
    $metadata[$key] = $m->$key;
  }

  // load uncommonly used metadata
  $results = db_query("SELECT * FROM {media_metadata} WHERE mid=%d", $mid);
  while ($obj = db_fetch_object($results)) {
    $value = unserialize($obj->value);
    $metadata[$obj->type] = ($value !== FALSE ? $value : $obj->value);
  }

  // load commonly used metadata
  $extras = db_fetch_array(db_query("SELECT * FROM {media_extras} WHERE mid=%d", $mid));
  if (count($extras)) {
    $extras['info'] = unserialize($extras['info']);
    $metadata = array_merge($metadata, $extras);
  }

  // allow modules to modify their metadata
  $metadata['mid'] = $mid;
  media_invoke_metadata('load', $metadata);
  unset($metadata['mid']);

  $media[$mid] = $metadata;

  return $metadata;
}

/**
 * Save the metadata for the item.
 *
 * @param $mid
 *   The media identifier.
 * @param $metadata
 *   The metadata information as an array
 */
function media_metadata_save($mid, $metadata) {
  if (is_array($metadata)) {
    // allow modules to modify their metadata, before saving.
    $metadata['mid'] = $mid;
    media_invoke_metadata('save', $metadata);
    unset($metadata['mid']);

    // the inbuilt metadata
    $inbuilt = array(
      'title' => !empty($metadata['title']) ? drupal_substr($metadata['title'], 0, 255) : '',
      'summary' => !empty($metadata['summary']) ? drupal_substr($metadata['summary'], 0, 255) : '',
      'source' => !empty($metadata['source']) ? drupal_substr($metadata['source'], 0, 255) : '',
      'reference' => !empty($metadata['reference']) ? drupal_substr($metadata['reference'], 0, 255) : '',
      'created' => !empty($metadata['created']) && is_numeric($metadata['created']) ? $metadata['created'] : time(),
      'expire' => !empty($metadata['expire']) && is_numeric($metadata['expire']) ? $metadata['expire'] : 0,
    );

    // generate the correct query for saving the data, while removing from metadata
    $sql = array();
    foreach ($inbuilt as $key => $value) {
      if (is_numeric($value)) {
        $sql[] = $key .'= %d';
      }
      else {
        $sql[] = $key ."= '%s'";
      }
      unset($metadata[$key]);
    }

    // run update query
    $inbuilt[] = $mid;
    db_query("UPDATE {media} SET ". implode(', ', $sql) ." WHERE mid=%d", $inbuilt);

    // serialize the standard metadata
    $standard = array(
      'mid' => $mid,
      'licence' => (isset($metadata['licence']) ? $metadata['licence'] : NULL),
      'description' => (isset($metadata['description']) ? $metadata['description'] : NULL),
      'bitrate' => (isset($metadata['bitrate']) ? drupal_substr($metadata['bitrate'], 0, 255) : NULL),
      'quality' => (isset($metadata['quality']) ? drupal_substr($metadata['quality'], 0, 255) : NULL),
      'length' => (isset($metadata['length']) ? drupal_substr($metadata['length'], 0, 255) : NULL),
      'info' => serialize(isset($metadata['info']) ? $metadata['info'] : array()),
    );

    // just in case something already exists
    if (db_result(db_query("SELECT eid FROM {media_extras} WHERE mid=%d", $mid))) {
      $values = array();
      foreach (array_keys($standard) as $item) {
        $values[] = $item ."='%s'";
      }
      $mid = array_shift($standard);
      array_push($standard, $mid);
      db_query("UPDATE {media_extras} SET ". implode(', ', $values) .' WHERE mid = %d', $standard);
    }
    else {
      db_query("INSERT INTO {media_extras} (". implode(', ', array_keys($standard)) .") VALUES(%d,". implode(', ', array_pad(array(), count($standard)-1, "'%s'")) .")", $standard);
    }

    // remove all the extra metadata which is not common.
    foreach (array_keys($standard) as $key) {
      unset($metadata[$key]);
    }

    // wipe unnecessary metadata
    $metadata = array_filter($metadata);

    $sql = array();
    $values = array();
    foreach ($metadata as $key => $value) {
      if (!is_numeric($value) && !is_string($value)) {
        $value = serialize($value);
      }
      $values[] = $mid;
      $values[] = $key;
      $values[] = $value;
      $sql[] = "(%d, '%s', '%s')";
    }

    // remove old ones
    db_query("DELETE FROM {media_metadata} WHERE mid=%d", $mid);

    // add new ones
    if (count($sql)) {
      db_query("INSERT INTO {media_metadata} (mid, type, value) VALUES ". implode(', ', $sql), $values);
    }

    media_metadata_load($mid, TRUE);
  }
}

/**
 * Retrieve the list of metadata keys used within the database.
 * This does not include the inbuilt metadata within the media table.
 *
 * @return
 *   Returns array of keys used for metadata within the database
 */
function media_metadata_used($refresh = FALSE) {
  static $list = array();

  if (!count($list) || $refresh) {
    // standard metadata from the extras table
    $list = array('title', 'summary', 'source', 'reference', 'created', 'expire', 'licence', 'description', 'bitrate', 'quality', 'length', 'info');

    // get the generated detail information
    $results = db_query("SELECT DISTINCT(type) FROM {media_metadata} ORDER BY type ASC");
    while ($obj = db_fetch_object($results)) {
      $list[] = $obj->type;
    }
  }

  return $list;
}

/**
 * Check for existance of a media file by the same as another in a folder.
 *
 * @param $name
 *   Name for media file.
 * @param $fid
 *   The folder identifier.
 * @param $ignore
 *   Gives us the ability to ignore an edited item
 * @return
 *   Returns TRUE on success, FALSE on failure.
 */
function media_exists($name, $fid, $ignore = FALSE) {
  return (db_result(db_query("SELECT mid FROM {media} WHERE name='%s' AND fid=%d AND mid<>%d", $name, $fid, $ignore)) !== FALSE);
}

/**
 * Retrieve the public filename for the item.
 *
 * @param $media
 *   The media object.
 * @return
 *   Returns generated filename.
 */
function media_filename($media, $options = array()) {
  $workspace = !empty($options['workspace']) ? $options['workspace'] : 'mmedia';
  $public = !empty($options['public']) ? $options['public'] : FALSE;
  
  if (!mapi_directory_check_location($media->path, $workspace) || (!$media->public && !$public)) {
    return 'media/'. $media->mid .'/transfer';
  }

  // returns nothing if media is not an object, and everything is empty.
  if (!is_object($media) || (!$media->path && !$media->name && !$media->ext)) {
    return '';
  }

  return $media->path . $media->name .'.'. $media->ext;
}

/**
 * Provide the folder path for the media.
 *
 * @param $media
 *   A media object
 * @param $refresh
 *   Forces refresh of directory buffering.
 */
function media_folder($media, $workspace = 'mmedia', $refresh = FALSE) {
  static $paths;

  // free memory if necessary
  if ($refresh || !isset($paths)) {
    $paths = array();
  }

  // only works if the media is installed
  if (!is_object($media)) {
    return FALSE;
  }

  // only call the database if it is not already loaded
  if (!array_key_exists($media->fid, $paths)) {
    $paths[$media->fid] = mapi_workspace_path($workspace, 'base') .'/'. db_result(db_query("SELECT path FROM {media_folders} WHERE fid=%d", $media->fid));
  }

  return $paths[$media->fid];
}

/**
 * Provides the default profile for a particular context.
 */
function media_default_profile($context, $profile) {
  if ($profile && in_array($profile, mapi_profile_list())) {
    return $profile;
  }
  switch ($context['context']) {
    case 'teaser':
      $profile = variable_get('mmedia_teaser_profile', NULL);
      break;
    case 'body':
      $profile = variable_get('mmedia_node_profile', NULL);
      break;
  }

  if (!$profile) {
    $profile = variable_get('mapi_profile_default', NULL);
  }

  return $profile;
}

/**
 * Compare media files to one another, checking for various attributes are equivalent.
 */
function media_compare_files($file1, $file2, $attributes = array()) {
  if (empty($attributes)) {
    return FALSE;
  }

  // must match extension
  if (mapi_file_ext($file1) != mapi_file_ext($file2)) {
    return FALSE;
  }

  // now check general attributes within the system
  $dim1 = mapi_extension_dimensions($file1);
  $dim2 = mapi_extension_dimensions($file2);
  foreach ($attributes as $key) {
    if ($dim1[$key] != $dim2[$key]) {
      return FALSE;
    }
  }

  return TRUE;
}
