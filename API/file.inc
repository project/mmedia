<?php

/**
 *  Get temporary path
 */
function media_directory_temp() {
  return mapi_workspace_path('mmedia', 'temp') .'/';
}

/**
 *  Get public path
 */
function media_directory_public() {
  return mapi_workspace_path('mmedia', 'base') .'/';
}

/**
 *  Get private path
 */
function media_directory_private() {
  return mapi_workspace_path('mmedia', 'private') .'/';
}
