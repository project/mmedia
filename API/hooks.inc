<?php

/**
 *  Invokes the hook_media().
 */
function media_invoke_item($op, &$media, $data = null) {
  return media_invoke('', $op, $media, $data);
}

/**
 *  Invokes the hook_media_folder().
 */
function media_invoke_folder($op, &$folder, $data = null) {
  return media_invoke('_folder', $op, $folder, $data);
}

/**
 *  Invokes the hook_media_metadata().
 */
function media_invoke_metadata($op, &$metadata, $data = null) {
  return media_invoke('_metadata', $op, $metadata, $data);
}

/**
 *  Invokes the hook_media_menu()
 */
function media_invoke_menu($op = null, $item = null) {
  $results = array();
  $options = module_invoke_all('media_menu', $op, $item);
  foreach ($options as $key => $value) {
    if (!empty($value['path'])) {
      // defaults to MENU_NORMAL_ITEM
      $value['type'] = (isset($value['type']) ? $value['type'] : MENU_NORMAL_ITEM);

      // make sure access is set to something
      $value['access'] = (isset($value['access']) ? $value['access'] : false);

      // default callback is drupal_get_form, this is possible to override
      $value['callback'] = (isset($value['callback']) ? $value['callback'] : 'drupal_get_form');

      // allows a simple string for callback arguments in hook menu, defaults to no extra arguments
      $value['callback arguments'] = (is_string($value['callback arguments']) ? array($value['callback arguments']) : $value['callback arguments']);

      // valid menu item
      $results[$key] = $value;
    }
  }
  return $results;
}

/**
 *  Invokes media related hooks
 */
function media_invoke($hook, $op, &$item, $data = null) {
  $return = array();
  foreach (module_implements('media'. $hook) as $name) {
    $function = $name .'_media'. $hook;
    $result = $function($op, $item, $data);
    // otherwise just return results
    if (isset($result) && is_array($result)) {
      $return = array_merge($return, $result);
    }
    else if (isset($result)) {
      $return[] = $result;
    }
  }
  return $return;
}
