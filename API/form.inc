<?php

/**
 *  Basic form processing for a media file.
 */
function _mmedia_form($edit = null) {
  global $form_values;

  $form['#media'] = $edit;

  // standard values
  $form['mid'] = array('#type' => 'value', '#value' => $edit->mid);
  $form['fid'] = array('#type' => 'value', '#value' => $edit->fid);
  $form['uid'] = array('#type' => 'value', '#value' => $edit->uid);
  $form['type'] = array('#type' => 'value', '#value' => $edit->type);

  // so we can check it hasn't already been changed
  $form['changed'] = array('#type' => 'hidden', '#value' => $edit->changed);

  // define title
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#required' => true,
    '#maxlength' => 255,
    '#default_value' => $edit->title,
    '#weight' => -10
  );

  // deal with filenames
  $form['filename'] = array('#type' => 'value', '#value' => $edit->filename);

  // upload capability
  if ($edit->filename) {
    // show the default for full.
    $profile = variable_get('mmedia_preview_profile', null);
    $profile = ($profile && in_array($profile, mapi_profile_list()) ? array('profile' => $profile) : array());

    $form['media'] = array('#value' => mapi_display($edit->filename, $profile), '#weight' => -8);
  }
  else {
    $form['media'] = array(
      '#type' => 'file',
      '#title' => t('Upload'),
      '#description' => t('Select a file to upload.'),
      '#weight' => -8
    );
    if (mapi_persist_get($edit->form_token)) {
      $form['media']['#description'] = '<strong>'. t('Uploading a new file will replace the one already uploaded.') .'<strong>';
    }

    // uploadable form
    $form['#attributes'] = array('enctype' => 'multipart/form-data');
  }

  // metadata information
  $form['metadata'] = array(
    '#type' => 'fieldset',
    '#title' => t('Metadata'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true
  );

  // get the form details for the metadata
  if ($meta = media_invoke_metadata('form', $edit->metadata, $edit)) {
    $form['metadata'] = array_merge($form['metadata'], $meta);
  }

  // make sure there is a preview shown if it exists
  $form['#after_build'] = array('_mmedia_form_preview');

  return $form;
}

function _mmedia_form_process($form_values, $temp = null, $workspace = 'mmedia') {
  if (($file = file_save_upload('media', array(), file_directory_temp()))) {
    // make sure that the temporary directory is existant.
    if (!$temp) {
      $temp = media_directory_temp();
    }

    // moves all files uploaded to the temporary directory
    $name = mapi_file_name($file->filepath);
    $ext = mapi_file_ext($file->filepath);

    $type = $form_values['type'];
    $ftype = mapi_extension_type($ext);

    if ($type && $type != $ftype) {
      file_delete($file->filepath);
      form_set_error('media', t('Invalid media type must be of type [%type].', array('%type' => $type)));
      return false;
    }

    // rename potentially problematic characters
    $name = preg_replace('|[^\w\d\-\_\.]+|', '_', $name);

    // rename potentially exploitable files
    if (in_array($ext, array('php', 'pl', 'py', 'cgi', 'asp', 'js', 'htm', 'html'))) {
      $ext .= '_';
    }

    // now that we have it safely named it
    if (!($filename = mapi_file_move($file->filepath, $temp . $name .'.'. $ext, array('replace' => FILE_EXISTS_RENAME, 'workspace' => $workspace)))) {
      mapi_error("Unable to rename file correctly from ($file->filepath) to ($temp$name.$ext)");
      file_delete($file->filepath);
      return false;
    }
    $file->filepath = $filename;

    // remove previous views
    if ($filename = mapi_persist_get($form_values['form_token'])) {
      file_delete($filename);
    }

    // save the preview
    mapi_persist_set($form_values['form_token'], $file->filepath);
  }

  // check for a preview if no media id provided
  if (!$form_values['mid'] && !mapi_persist_get($form_values['form_token'])) {
    form_set_error('media', t('A media file is required.'));
  }

  // check metadata
  $meta = _mmedia_form_flatten($form_values['metadata']);
  media_invoke_metadata('validate', $meta);
}

function _mmedia_form_post($form_values, $workspace = 'mmedia') {
  // should take final thing and create/update media file from it.
  $mid = $form_values['mid'];
  if ($mid) {
    $media = media_load($mid);
    if ($media->mid && ($media->title != $form_values['title'])) {
      media_rename($media->mid, $form_values['title'], $workspace);
    }
  }
  else {
    $parent = ($form_values['parent'] ? $form_values['parent'] : 0);
    $media = media_create(mapi_persist_get($form_values['form_token']), $form_values['title'], $parent);
  }

  // modify and save the metadata
  $meta = _mmedia_form_flatten($form_values['metadata']);
  $meta['title'] = $form_values['title'];
  media_metadata_save($media->mid, $meta);

  // remove any possible previews for form
  if ($filename = mapi_persist_get($form_values['form_token'])) {
    mapi_file_delete($filename, $workspace);
    mapi_persist_set($form_values['form_token'], null);
  }

  return $media;
}

/**
 *  Media Preview section.
 */
function _mmedia_form_preview($form, &$form_state) {
  $op = $form['#post']['op'];

  if ($op == t('Preview')) {
    drupal_validate_form($form['form_id']['#value'], $form, $form_state);
    if (!form_get_errors()) {
      // first see if something already exists, otherwise try the form itself
      if (($filename = $form['filename']['#value']) || ($filename = mapi_persist_get($form['form_token']['#value']))) {
        $profile = variable_get('mmedia_preview_profile', null);
        $profile = ($profile && in_array($profile, mapi_profile_list()) ? array('profile' => $profile) : array());

        $form['media']['#description']  = '<strong>'. t('Uploading another file will cancel this one.') .'</strong>';
        $form['media']['#suffix'] = '<div class="media-preview">'. mapi_display($filename, $profile) .'</div>';
      }
    }
  }

  return $form;
}

/**
 *  Used for flattening out the form_values of the metadata
 */
function _mmedia_form_flatten($form_values) {
  $list = array();
  if (is_array($form_values)) {
    foreach ($form_values as $key => $value) {
      if (is_array($value)) {
        $list = array_merge($list, _mmedia_form_flatten($value));
      }
      else {
        $list[$key] = $value;
      }
    }
  }
  return $list;
}

/**
 *  Folder edit form
 */
function _mmedia_folder_form($edit = null) {
  $form['#folder'] = $edit;

  // basic values which need to be passed over to this
  $form['fid'] = array('#type' => 'value', '#value' => $edit->fid);
  $form['uid'] = array('#type' => 'value', '#value' => $edit->uid);
  $form['parent'] = array('#type' => 'value', '#value' => $edit->parent);

  // name
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => true,
    '#maxlength' => 255,
    '#default_value' => $edit->name
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  if ($edit->fid) {
    $form['delete'] = array('#type' => 'button', '#value' => t('Delete'));
  }

  return $form;
}

/**
 *  Process the data for the folder edit form.
 */
function _mmedia_folder_process($form_values) {
  // checks that name exists
  if ($form_values['name'] && preg_match('/[^\w\d_\- ]+/', $form_values['name'])) {
    form_set_error('name', t('Name can only contain alphanumeric, space, hyphen and underscore characters.'));
  }
  elseif (mmedia_folder_exists($form_values['name'], (int)$form_values['parent'], (int)$form_values['fid'])) {
    form_set_error('name', t('Folder of that name already exists.'));
  }
  elseif ($form_values['op'] == t('Delete')) {
    $folders = _mmedia_folder_cache();
    if (array_key_exists($form_values['fid'], $folders)) {
      $folder = $folders[$form_values['fid']];
      if (is_array($folder)) {
        drupal_goto(FOLDER_PATH .'/'. $folder['line'] . 'delete');
        exit();
      }
    }
  }
}

/**
 *  Post the data for the folder.
 */
function _mmedia_folder_post($form_values) {
  // simply rename the folder
  if ($fid = $form_values['fid']) {
    $folder = mmedia_folder_load($fid);
    mmedia_folder_rename($fid, $form_values['name']);
  }
  // otherwise create a new folder
  else {
    $folder = mmedia_folder_create((int)$form_values['parent'], $form_values['name']);
  }
  return $folder;
}

