<?php

/**
 * Loads a folder from the database.
 *
 * @param $fid
 *   The folders identifier.
 * @param $refresh
 *   Allows refresh of the folders memory.
 * @return
 *   Returns false on failure, folder object on success.
 */
function mmedia_folder_load($fid, $refresh = false) {
  static $folders;

  // clear the cache
  if ($refresh || !isset($folders)) {
    $folders = array();
  }

  // check if valid fid
  if (!is_numeric($fid)) {
    return false;
  }

  // check cache
  if (array_key_exists($fid, $folders)) {
    return $folders[$fid];
  }

  // load from database
  if ($fid) {
    $f = db_fetch_object(db_query("SELECT * FROM {media_folders} WHERE fid=%d", $fid));
  }
  else {
    $f = new stdClass;
    $f->name = 'none';
    $f->fid = 0;
    $f->parent = 0;
    $f->lineage = '';
    $f->path = '';
    $f->depth = 0;
  }

  // invalid folder
  if (!$f) {
    return $f;
  }

  // notify modules of the loading
  media_invoke_folder('load', $f);

  // save this as cached
  $folders[$fid] = $f;

  return $folders[$fid];
}

/**
 * Checks for existance of a folder by the same as another in a folder.
 *
 * @param $name
 *   Name for folder.
 * @param $fid
 *   The parent folder identifier.
 * @param $ignore
 *   Gives us the ability to ignore an edited item
 * @return
 *   Returns true on success, false on failure.
 */
function mmedia_folder_exists($name, $fid, $ignore) {
  return (db_result(db_query("SELECT fid FROM {media_folders} WHERE name='%s' AND parent=%d AND fid <> %d", $name, $fid, $ignore)) !== false);
}

/**
 * Moves the folders to the correct location.
 *
 * @param $src_id
 *   The source folder identifier.
 * @param $dest_id
 *   The destination folder identifier.
 * @return
 *   Return false on failure, true on success.
 */
function mmedia_folder_move($src_id, $dest_id, $workspace = 'mmedia') {
  // load folders
  if (!($source = mmedia_folder_load($src_id)) || !($dest = mmedia_folder_load($dest_id))) {
    return false;
  }

  // no need to move it, if it is the same directory, or creates circular connection
  if (($source->fid && $dest->lineage && strpos('/'. $source->fid .'/', $dest->lineage) !== false)) {
    return false;
  }

  // check for existance of folder already in that folder with same name
  if (db_result(db_query("SELECT fid FROM {media_folders} WHERE parent=%d AND name='%s' AND fid <> %d", $dest->fid, $source->name, $src_id))) {
    mapi_error('Directory of that name already exists.', 'media');
    return false;
  }

  // update the actual folder
  $lineage = $dest->lineage . ($dest->lineage ? '' : '/') ."{$dest->fid}/";
  $depth = $dest->depth + 1;
  $path = $dest->path . $source->name .'/';
  db_query("UPDATE {media_folders} SET parent=%d, lineage='%s', depth=%d, path='%s' WHERE fid=%d", $dest->fid, $lineage, $depth, $path, $source->fid);

  $path = media_directory_public() . $path;

  // make sure the path exists
  if (!mapi_directory_check($path, array('create' => true, 'workspace' => $workspace))) {
    return FALSE;
  }

  // clear the caches
  mmedia_folder_load(null, true);
  _mmedia_folder_cache(true);

  // move all the media paths to reflect the shifts.
  $results = db_query("SELECT mid FROM {media} WHERE fid = %d", $source->fid);
  while ($obj = db_fetch_object($results)) {
    $media = media_load($obj->mid);
    $filename = media_filename($media);
    if (!mapi_file_move($filename, $path . $media->name .'.'. $media->ext)) {
      mapi_error('Unable to move file (%source) to (%dest)', array('%source' => $filename, '%dest' => $path . $media->name .'.'. $media->ext));
    }

    // make sure things get notified
    $media->old_ext = $media->ext;
    $media->old_name = $media->name;
    $media->old_path = $media->path;
    $media->path = $path;
    $media->old_filename = $filename;

    // notify others that the media has been moved
    media_invoke_item('move', $media);
  }
  db_query("UPDATE {media} SET path='%s' WHERE fid=%d", $path, $source->fid);

  // move subfolders
  $results = db_query("SELECT fid FROM {media_folders} WHERE parent = %d", $source->fid);
  while ($obj = db_fetch_object($results)) {
    mmedia_folder_move($obj->fid, $source->fid, $workspace);
  }

  // clear other caches
  cache_clear_all('*', 'cache_menu', true);
  _mmedia_folder_cache(true);

  // unlink the directory, there shouldn't be anything left of it.
  mapi_directory_delete(media_directory_public() . $source->path, false, $workspace);

  // notify modules of change to folder
  $source->old_parent  = $source->parent;
  $source->old_lineage = $source->lineage;
  $source->old_depth   = $source->depth;
  $source->old_path    = $source->path;
  $source->parent      = $dest->fid;
  $source->lineage     = $lineage;
  $source->depth       = $depth;
  $source->path        = $dest->path . $source->name .'/';
  media_invoke_folder('move', $source);

  return true;
}

/**
 * Renames the folder.
 *
 * @param $src_id
 *   The folder identifier.
 * @param $name
 *   The new name to rename it to.
 * @return
 *   Return false on failure, true on success.
 */
function mmedia_folder_rename($src_id, $name, $workspace = 'mmedia') {
  // load the folder
  if (!($source = mmedia_folder_load($src_id))) {
    return false;
  }

  // don't need to change it, if the name is the same
  if ($source->name == $name) {
    return true;
  }

  // There a folder of the same name in the same directory
  if (db_result(db_query("SELECT fid FROM {media_folders} WHERE parent=%d AND name='%s' AND fid <> %d", $source->parent, $name, $source->fid))) {
    mapi_error('Directory of that name already exists', 'media');
    return false;
  }

  // get the new path name
  $base = mmedia_folder_load($source->parent);
  $path = $base->path . $name .'/';

  // physically rename it
  if (!mapi_directory_move(media_directory_public() . $source->path, media_directory_public() . $path, $workspace)) {
    mapi_error('Unable to rename directory (%source) to (%dest)', array('%source' => media_directory_public() . $source->path, '%dest' => media_directory_public() . $path), 'media');
    return false;
  }

  // now change all the path names for the child folders
  $results = db_query("SELECT fid, path FROM {media_folders} WHERE path LIKE '%s%%'", $source->path);
  while ($obj = db_fetch_object($results)) {
    // change the name, using preg_replace to guarantee the change from the beginning only
    $new_path = preg_replace('|^'. preg_quote($source->path) .'|', $path, $obj->path);

    // check that it's not the same directory
    if ($new_path != $obj->path) {
      db_query("UPDATE {media_folders} SET path='%s' WHERE fid=%d", $new_path, $obj->fid);
    }
  }

  // change the name of the folder, and the path it represents
  db_query("UPDATE {media_folders} SET name='%s', path='%s' WHERE fid=%d", $name, $path, $source->fid);

  // clear the media_folder cache
  media_folder(null, $workspace, true);
  cache_clear_all('*', 'cache_menu', true);
  _folder_cache(true);

  $source->old_name = $source->name;
  $source->name = $name;
  $source->path = $path;
  media_invoke_folder('rename', $source);

  return true;
}

/**
 * Creates a folder within the parent directory.
 *
 * @param $parent
 *   The folder identifier for the parent of the to be created directory.
 * @param $name
 *   The name of the folder.
 * @return
 *   Return false on failure, a folder object on success.
 */
function mmedia_folder_create($parent, $name, $workspace = 'mmedia') {
  global $user;

  // load parent, if exists
  if (!($source = mmedia_folder_load($parent))) {
    return false;
  }

  // check if directory does not already exist
  if (db_result(db_query("SELECT fid FROM {media_folders} WHERE parent=%d AND name='%s'", $source->fid, $name))) {
    mapi_error('Folder of that name already exists.', 'media');
    return false;
  }

  // We need to make sure that the paths are always relative paths, i.e. no / in front.
  // create the tree system
  $path = $source->path . $name .'/';

  // create the hierarchical path
  if (!mapi_directory_create(media_directory_public() . $path, false, $workspace)) {
    mapi_error('Unable to create directory (%dir)', array('%dir' => media_directory_public() . $path), 'media');
    return false;
  }

  // create the correct lineage
  $lineage = '';
  if ($source->fid) {
    $lineage = $source->lineage . ($source->lineage ? '' : '/') ."{$source->fid}/";
  }

  // set folder data and
  $folder = array('uid' => $user->uid, 'parent' => $source->fid,
    'name' => $name, 'depth' => $source->depth + 1, 'lineage' => $lineage,
    'path' => $path
  );
  $folder_values = array('uid' => '%d', 'parent' => '%d',
    'name' => "'%s'", 'depth' => '%d', 'lineage' => "'%s'", 'path' => "'%s'"
  );

  // add this folder to the database
  db_query('INSERT INTO {media_folders} ('. implode(', ', array_keys($folder)) .') VALUES ('. implode(', ', $folder_values) .')', $folder);
  $fid = db_last_insert_id('media_folders', 'fid');
  $folder['fid'] = $fid;

  cache_clear_all('*', 'cache_menu', true);
  _mmedia_folder_cache(true);

  // notify the modules of the creation of the folder
  $folder = (object)$folder;
  media_invoke_folder('create', $folder);

  // this notifies on load
  $folder = mmedia_folder_load($fid);

  return $folder;
}

/**
 * Creates a folder within the parent directory.
 *
 * @param $src_id
 *   The folder identifier.
 * @return
 *   Return false on failure, true on success.
 */
function mmedia_folder_delete($fid, $delete = false, $workspace = 'mmedia') {
  // load the folder to delete
  if (!($source = mmedia_folder_load($fid))) {
    return false;
  }

  // go through all children folders
  $results = db_query("SELECT fid FROM {media_folders} WHERE lineage LIKE '%%/%d/%%' ORDER BY depth DESC", $source->fid);
  while ($obj = db_fetch_object($results)) {
    mmedia_folder_delete($obj->fid, $delete, $workspace);
  }

  // only move the media if there is something to move
  $test = db_result(db_query("SELECT COUNT(*) FROM {media} WHERE fid=%d", $source->fid));
  if ($test) {
    // got through all media located in folder
    $results = db_query("SELECT mid FROM {media} WHERE fid=%d", $source->fid);
    while ($obj = db_fetch_object($results)) {
      // delete if that's the policy
      if ($delete) {
        media_delete($obj->mid, $workspace);
      }
      // otherwise move to parent folder
      else {
        media_move($obj->mid, $source->parent, $workspace);
      }
    }
  }

  // update database
  db_query("DELETE FROM {media_folders} WHERE fid=%d", $source->fid);

  // clear the media_folder cache
  media_folder(null, $workspace, true);
  cache_clear_all('*', 'cache_menu', true);
  _mmedia_folder_cache(true);

  // notify the modules of folder change
  media_invoke_folder('delete', $source);

  // remove the physical folder
  if (!mapi_directory_delete(media_directory_public() . $source->path, false, $workspace)) {
    mapi_error('Unable to delete directory (%directory).', array('%directory' => media_directory_public() . $source->path), 'media');
  }

  return true;
}

/**
 * Creates the list of subfolders for a folder
 *
 * @param $fid
 *   The folder identifier.
 * @param $uid
 *   Specifies the user id which has access to these subfolders. 0 means all accessible.
 * @return
 *   Returns an associative array, using the sub-folder identifier to reference the name.
 */
function mmedia_folder_subfolders($fid) {
  $children = array();

  $folders = _mmedia_folder_cache();
  if (array_key_exists($fid, $folders)) {
    foreach ($folders[$fid]['children'] as $cfid) {
      $children[$cfid] = $folders[$cfid]['name'];
    }
  }
  return $children;
}

/**
 * Returns the items contained within the folder, based on permissions for user.
 *
 * @param $fid
 *   The folder identifier.
 * @param $uid
 *   Specifies the user identifer for the
 * @return
 *   Returns an associative array, using the sub-folder identifier to reference the name.
 */
function mmedia_folder_items($fid, $uid = 0) {
  $media = array();
  $results = db_query("SELECT mid, name FROM {media} WHERE fid=%d" . ($uid ? ' AND uid=%d' : ''), $fid, $uid);
  while ($obj = db_fetch_object($results)) {
    $media[$obj->mid] = $obj->name;
  }
  return $media;
}

/**
 * Returns a list of the children within a folder.
 *
 * @param $fid
 *   The folder identifier.
 * @param $paging
 *   Provides both for paging and what element is currently in use.
 * @return
 *   Returns array of media identifier associated names of the entire tree
 */
function mmedia_folder_children($fid, $paging = false) {
  $children = array();
  $sql = "SELECT m.mid, m.title FROM {media} m WHERE fid=%d ORDER BY m.title ASC";
  $results = ($paging ? pager_query($sql, variable_get('mmedia_folder_page', 10), $paging, null, array($fid)) : db_query($sql, $fid));
  while ($obj = db_fetch_object($results)) {
    $children[$obj->mid] = $obj->title;
  }
  return $children;
}

/**
 * Returns the entire subtree as a flat array of folder identifiers and names.
 *
 * @param $fid
 *   The folder identifier.
 * @return
 *   Returns array of folder identifier associated names of the entire tree
 *   including the folder itself.
 */
function mmedia_folder_branches($fid) {
  $branches = array();
  $folders = _mmedia_folder_cache();
  if (array_key_exists($fid, $folders)) {
    $branches[$fid] = $folders[$fid]['name'];
    foreach ($folders[$fid]['children'] as $cfid) {
      $branches = array_merge($branches, mmedia_folder_branches($cfid));
    }
  }

  return $branches;
}

/**
 *  Returns the fid of the parent folder.
 *
 *  @param $fid
 *    Folder ID.
 *  @return
 *    Returns either the parent folder ID, or FALSE if none exists.
 */
function mmedia_folder_parent($fid) {
  return db_result(db_query("SELECT parent FROM {media_folders} WHERE fid=%d", $fid));
}

/**
 *  Provide a list of the parents of the given FID
 *
 *  @param $fid
 *    Folder ID.
 *  @return
 *    Returns an array with a list of the parent FIDs.
 */
function mmedia_folder_parents($fid) {
  $lineage = db_result(db_query("SELECT lineage FROM {media_folders} WHERE fid=%d", $fid));
  if (!$lineage) {
    return array();
  }
  // remove all the extras after exploding into an array
  return array_filter(explode('/', $lineage));
}

/**
 *  Determines whether folder is a direct child of a given parent.
 *
 *  @param $fid
 *    Folder ID.
 *  @return
 *    Returns TRUE or FALSE.
 */
function mmedia_folder_is_child($fid, $pid) {
  return ($pid === db_result(db_query("SELECT parent FROM {media_folders} WHERE fid=%d", $fid)));
}

/**
 *  Determines whether folder is a branch of a given parent.
 *
 *  @param $fid
 *    Folder ID.
 *  @return
 *    Returns TRUE or FALSE.
 */
function mmedia_folder_is_branch($fid, $pid) {
  $parents = mmedia_folder_parents($fid);
  return in_array($pid, $parents);
}

/**
 *  Loads or generates the cached version of the media folders.
 *  The depth calculated
 *
 *  @param $refresh
 *    Boolean, sets if it loads or generates the folders cache.
 */
function _mmedia_folder_cache($refresh = false) {
  static $folders;
  global $user;

  if (!isset($folders)) {
    $cache = cache_get('media_folders:'. $user->uid, 'cache_media');
    if ($cache->data) {
      $folders = unserialize($cache->data);
    }
  }

  if ($refresh || !isset($folders)) {
    // start with base folder
    $folders = array(
      'line' => array('' => 0),
      0 => array('parent' => 0, 'name' => 'none', 'children' => array())
    );

    // what depth to load up to
    $sql = "SELECT fid, name, lineage, path FROM {media_folders} ";
    if (user_access('view own folder') && $user->uid != 1) {
      $sql .= " WHERE uid=%d";
    }
    $sql .= "ORDER BY depth, name ASC";
    $results = db_query($sql, $user->uid);

    // allows check by folder
    if (user_access('view folder') || !user_access('view own folder')) {
      // load all visible objects
      while ($object = db_fetch_object($results)) {
        // now we determine what the closest
        $parts = array_filter(explode('/', $object->lineage));
        while ($fid = array_pop($parts)) {
          if (array_key_exists($fid, $folders)) {
            break;
          }
        }
        $folders[$object->fid] = array('parent' => $fid, 'name' => $object->name, 'children' => array());
        $folders[(int)$fid]['children'][] = $object->fid;
        $folders['line'][$object->path] = $object->fid;
      }
      ksort($folders['line']);
    }

    // set the cache for the user
    cache_set('media_folders:'. $user->uid, serialize($folders), 'cache_media', CACHE_TEMPORARY);
  }

  return $folders;
}
