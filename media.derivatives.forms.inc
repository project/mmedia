<?php

/**
 * Listing of the derivatives for the media.
 */
function mmedia_derivatives_list_form(&$form_state, $media) {
  // make sure that it's an actual media we're loading.
  $media = media_load($media->mid);
  if (!$media) {
    drupal_not_found();
    exit();
  }

  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  drupal_set_breadcrumb($breadcrumbs);

  $derivatives = mapi_derivative_list_by_extension($media->ext);
  $files = mapi_generated_load(media_filename($media));

  $rows = array();
  foreach ($derivatives as $did => $name) {
    $found = array_key_exists($did, $files);
    $rows[] = array(
      l($name, MEDIA_PATH .'/'. $media->mid .'/derivatives/'. $did),
      $found && $files[$did]->override ? t('Overridden') : ($found ? t('Generated') : t('Free')),
    );
  }

  if (count($rows)) {
    $form['details'] = array(
      '#value' => t('This are the available derivatives for this media.') .
      theme('table', array(t('Derivative'), t('Status')), $rows)
    );
  }
  else {
    $form['details'] = array('#value' => t('There are no derivatives that can be generated for this media.'));
  }

  return $form;
}

/**
 *  Derivatives media form
 */
function mmedia_derivatives_form(&$form_state, $media, $derivative) {
  // make sure it's a valid media file
  $media = media_load($media->mid);

  // make sure that derivative fits into this media extension.
  $derivatives = mapi_derivative_list_by_extension($media->ext);
  if (!array_key_exists($derivative->did, $derivatives)) {
    drupal_not_found();
    exit();
  }

  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  $breadcrumbs[] = l(t('Derivatives'), MEDIA_PATH .'/'. $media->mid .'/derivatives');
  drupal_set_breadcrumb($breadcrumbs);

  // get the details of the generated derivative
  $details = mapi_generated_details(media_filename($media), $derivative->did);

  // basic necessities for other modules to create changes
  $form['mid'] = array('#type' => 'value', '#value' => $media->mid);
  $form['did'] = array('#type' => 'value', '#value' => $derivative->did);

  if ($details) {
    $form['preview'] = array('#value' => '<div class="media-derivative-preview">'. mapi_display($details['filename'], array('profile' => NULL)) .'</div>');
  }
  else {
    $form['preview'] = array('#value' => '<div>'. t('There is no generated derivative for this media.') .'</div>');
  }

  // delete derivative
  $form['delete'] = array('#type' => 'submit', '#value' => t('Delete Derivative'));

  // force generation of derivative
  $form['force'] = array('#type' => 'submit', '#value' => t('Force Generation'));

  // upload replacement
  $form['upload'] = array('#type' => 'submit', '#value' => t('Replace via Upload'));

  return $form;
}

/**
 *  Submission of media derivatives form.
 */
function mmedia_derivatives_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  switch ($values['op']) {
    case t('Replace via Upload'):
      drupal_goto(MEDIA_PATH .'/'. $values['mid'] .'/derivatives/'. $values['did'] .'/upload');
      exit();
      break;
    // deletes the derivative
    case t('Delete Derivative'):
      $media = media_load($values['mid']);
      mapi_generated_delete(media_filename($media), $values['did']);
      break;

    // forces a regeneration of the file if necessary
    case t('Force Generation'):
      $media = media_load($values['mid']);
      $derivative = mapi_derivative_name($values['did']);

      // check for already generated file, replace it.
      $filename = NULL;
      if ($generated = mapi_generated_details(media_filename($media), $derivative->did)) {
        $filename = $generated['filename'];
      }

      // generates the derivative
      mapi_derive(media_filename($media), $derivative, array('filename' => $filename, 'exists' => FILE_EXISTS_REPLACE));
      break;
  }
}

/**
 * Upload capability for individual derivative media modification.
 */
function mmedia_derivatives_upload_form(&$form_state, $media, $derivative) {
  $media = media_load($media->mid);

  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  $breadcrumbs[] = l(t('Derivatives'), MEDIA_PATH .'/'. $media->mid .'/derivatives');
  drupal_set_breadcrumb($breadcrumbs);

  $form['mid'] = array('#type' => 'value', '#value' => $media->mid);
  $form['did'] = array('#type' => 'value', '#value' => $derivative->did);
  $form['ext'] = array('#type' => 'value', '#value' => $derivative->output);

  // handle the processing of the media form...
  $form['media'] = array(
    '#type' => 'file',
    '#title' => t('Upload'),
    '#description' => t('Select a file to upload.') .' '. t('Must have the extension %ext.', array('%ext' => $derivative->output)),
  );

  // uploadable form
  $form['#attributes'] = array('enctype' => 'multipart/form-data');

  $form['submit'] = array('#type' => 'submit', '#value' => t('Upload'));

  return $form;
}

/**
 *  Validate the upload form.
 */
function mmedia_derivatives_upload_form_validate($form, &$form_state) {
  if ($file = file_save_upload('media', array(), file_directory_temp())) {
    // validate that the file is correct
    if (mapi_file_ext($file->filepath) != $form_state['values']['ext']) {
      file_delete($file->filepath);
      form_set_error('media', t('File does not have a valid extension. It must be (%ext).', array('%ext' => $form_state['values']['ext'])));
    }
    // store if correct
    else {
      // we are able to generate the file that is supposed to be at this location.
      $filename = media_filename(media_load($form_state['values']['mid']));
      $gen = mapi_generated_details($filename, $form_state['values']['did']);
      if ($gen && !media_compare_files($file->filepath, $gen['filename'], array('width', 'height'))) {
        file_delete($file->filepath);
        form_set_error('media', t('File does not match require generated settings.'));
        return;
      }
      $form_state['storage']['gen'] = $gen['filename'];
      $form_state['storage']['source'] = $filename;
      $form_state['storage']['file'] = $file;
    }
  }
  else {
    form_set_message('', t('No file uploaded'));
  }
}

/**
 * Submission of mmedia_derivatives_form().
 */
function mmedia_derivatives_upload_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $storage = $form_state['storage'];
  unset($form_state['storage']);

  // overwrite the generated file in the system settings.
  if ($storage['gen']) {
    if (!($filename = mapi_file_copy($storage['file']->filepath, $storage['gen'], array('replace' => FILE_EXISTS_REPLACE, 'workspace' => 'mmedia')))) {
      file_delete($storage['file']->filepath);
      drupal_set_message(t('Unable to move updated file to filename (%filename).', array('%filename' => $storage['gen'])), 'error');
      return;
    }
    file_delete($storage['file']->filepath);
  }
  // otherwise just move it to the correct location, with an appropriate rename.
  else {
    $temp = mapi_file_path($storage['source']) . mapi_file_name($storage['source']) .'-'. $values['did'] .'.'. $values['ext'];
    drupal_set_message($temp .' = '. is_file($temp));
    if (!($filename = mapi_file_move($storage['file']->filepath, $temp, array('replace' => FILE_EXISTS_RENAME, 'workspace' => 'mmedia')))) {
      file_delete($storage['file']->filepath);
      drupal_set_message(t('Unable to move updated file to filename (%filename).', array('%filename' => $temp)), 'error');
      return;
    }
    drupal_set_message($filename);
  }

  // rebuild the mapi_generated settings
  mapi_generated_override($storage['source'], $values['did'], $filename);

  $form_state['redirect'] = MEDIA_PATH .'/'. $values['mid'] .'/derivatives/'. $values['did'];
}
