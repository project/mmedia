<?php

/**
 *  Loads a media item with extra details, nid and mid.
 */
function media_nodes_load_item($nid, $refresh = false) {
  static $media_nodes;

  if (!isset($media_nodes)) {
    $media_nodes = array();
  }

  // return cached node details
  if (array_key_exists($nid, $media_nodes) && (!$refresh)) {
    return $media_nodes[$nid];
  }

  // get details from the database
  if (!($item = media_nodes_item($nid))) {
    return false;
  }

  // load media
  $media = media_load($item->mid);

  // set necessary reference materials for media
  $media->nid = $item->nid;
  $media->did = $item->did;

  $media_nodes[$nid] = $media;

  return $media;
}

/**
 *  Loads the node/media relationship
 */
function media_nodes_item($nid) {
  return db_fetch_object(db_query("SELECT * FROM {media_nodes} WHERE nid=%d", $nid));
}

/**
 *  Creates the node/media relationship
 */
function media_nodes_create($nid, $mid, $did = 0) {
  if (is_numeric($nid) && is_numeric($mid) && is_numeric($did)) {
    db_query("INSERT INTO {media_nodes} (nid, mid, did) VALUES (%d, %d, %d)", $nid, $mid, $did);
  }
}

/**
 *  Deletes the node/media relationship
 */
function media_nodes_delete($nid) {
  if (is_numeric($nid)) {
    db_query("DELETE FROM {media_nodes} WHERE nid=%d", $nid);
  }
}

/**
 * Creates a node from a media derivative
 *
 * @param $mid
 *   The media identifier
 * @param $did
 *   The derivative identifier
 * @return
 *   Returns a media_item_node() object, FALSE on failure.
 */
function media_nodes_item_create($mid, $did) {
  global $user;

  // check for correct details in the database
  if (!($media = media_load($mid))) {
    return FALSE;
  }

  //  check that the mapi_derivatives are loaded, and that the derivative exists
  if ($did) {
    if (!module_exists('mapi_derivatives') || !($derivative = mapi_derivative_load($did)) || ($derivative->ext != $media->ext)) {
      return FALSE;
    }
  }

  // check that no nodes already exist for media/derivative, if already exists, simply return that node
  $nid = db_result(db_query("SELECT nid FROM {media_nodes} WHERE mid=%d AND did=%d", $mid, $did));
  if ($nid) {
    $media->nid = $nid;
    $media->did = $did;
    return $media;
  }

  // Ok, now load metadata, which will be used for the node details
  $meta = media_metadata_load($media->mid);

  // make standard node
  $node = new stdClass();

  $node->title = $media->title;
  $node->body = (!empty($meta['description']) ? $meta['description'] : $meta['summary']);
  $node->teaser = (!empty($meta['summary']) ? $meta['summary'] : '');
  $node->type = 'media_'. $media->type;
  $node->uid = $user->uid;
  $node->status = variable_get('media_node_status', 1);
  $node->sticky = variable_get('media_node_sticky', 0);
  $node->promote = variable_get('media_node_promote', 0);

  node_save($node);

  if ($node->nid) {
    // mandatory notification message
    drupal_set_message(t("%title has been created.", array('%title' => $node->title)));

    // update the database
    db_query("INSERT INTO {media_nodes} (nid,did,mid) VALUES (%d,%d,%d)", $node->nid, $did, $mid);
  }

  $media->nid = $node->nid;
  $media->did = $did;

  return $media;
}

