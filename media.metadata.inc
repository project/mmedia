<?php

switch ($op) {
  case 'form':
    $edit = (object)$metadata;

    // textual information information
    $form['info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Textual Information'),
      '#collapsible' => true,
      '#collapsed' => false
    );

    $form['info']['summary'] = array(
      '#type' => 'textarea',
      '#title' => t('Summary'),
      '#default_value' => $edit->summary,
      '#maxlength' => 255,
      '#rows' => 2
    );

    if (variable_get('mmedia_metadata_description', false)) {
      $form['info']['description'] = array(
        '#type' => 'textarea',
        '#title' => t('Description'),
        '#default_value' => $edit->description,
      );
    }

    // licensing information, or source relative material
    $form['refinfo'] = array(
      '#type' => 'fieldset',
      '#title' => t('Reference Information'),
      '#collapsible' => true,
      '#collapsed' => false
    );

    $form['refinfo']['source'] = array(
      '#type' => 'textfield',
      '#title' => t('Source'),
      '#default_value' => $edit->source,
      '#maxlength' => 255
    );

    $form['refinfo']['reference'] = array(
      '#type' => 'textfield',
      '#title' => t('Reference'),
      '#default_value' => $edit->reference,
      '#maxlength' => 255
    );

    if (variable_get('mmedia_metadata_license', false)) {
      $form['refinfo']['licence'] = array(
        '#type' => 'textarea',
        '#title' => t('Licensing'),
        '#default_value' => $edit->licence,
      );
    }

    // date related information
    if (variable_get('mmedia_metadata_changed', false) && module_exists('date')) {
      $form['dateinfo']['changed'] = array(
        '#type' => 'date',
        '#title' => t('Changed'),
        '#default_value' => $edit->changed
      );
    }

    if (variable_get('mmedia_metadata_expire', false) && module_exists('date')) {
      $form['dateinfo']['expire'] = array(
        '#type' => 'date',
        '#title' => t('Expires'),
        '#default_value' => $edit->expire,
        '#description' => t('If expires is before or equal to changed, then it does not expire.')
      );
    }

    if (isset($form['dateinfo'])) {
      $form['dateinfo'] = array_merge($form['dateinfo'], array(
        '#type' => 'fieldset',
        '#title' => t('Date Information'),
        '#collapsible' => true,
        '#collapsed' => false
      ));
    }
  break;
}
