<?php

/**
 *  View form for folders
 */
function mmedia_folder_view_form(&$form_state, $folder) {
  drupal_add_css(drupal_get_path('module', 'mmedia') .'/mmedia.css');
  // always load base folder for null variable
  if (is_null($folder)) {
    $folder = mmedia_folder_load(0);
  }

  // remove current folder from the top of the list
  $breadcrumbs = _mmedia_set_breadcrumb($folder);
  array_pop($breadcrumbs);
  drupal_set_breadcrumb($breadcrumbs);

  // list the folders currently to the
  $subfolders = mmedia_folder_subfolders($folder->fid);
  $children = mmedia_folder_children($folder->fid);

  if ($pager = theme('pager', NULL, variable_get('mmedia_folder_page', 10), 1)) {
    $form['pager-top'] = array('#value' => '<div class="media-pager">'. $pager .'</div>');
  }

  if (!empty($subfolders)) {
    $form['subfolders'] = array(
      '#prefix' => '<div class="subfolders"><h3>'. t('Folders') .'</h3>',
      '#suffix' => '</div>',
    );
    foreach ($subfolders as $fid => $name) {
      $form['subfolders'][$fid] = array('#prefix' => '<div>', '#value' => l($name, FOLDER_PATH .'/'. $fid), '#suffix' => '</div>');
    }
  }

  // list the items currently available
  if (!empty($children)) {
    $form['children'] = array(
      '#prefix' => '<div class="children"><h3>'. t('Media') .'</h3>',
      '#suffix' => '</div>',
    );
    foreach ($children as $mid => $title) {
      $form['children'][$mid] = array('#prefix' => '<div>', '#value' => l($title, MEDIA_PATH .'/'. $mid), '#suffix' => '</div>');
    }
  }

  // add pager to the bottom
  if ($pager) {
    $form['pager-bottom'] = array('#value' => '<div class="media-pager">'. $pager .'</div>');
  }

  return $form;
}

/**
 *  Edit form for folders
 */
function mmedia_folder_edit_form(&$form_state, $folder, $parent) {
  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($parent->fid));

  // handles the folder editing process
  $edit = $folder;
  if (!$edit) {
    $edit = new stdClass;
    $edit->parent = $parent->fid;
  }
  else {
    $breadcrumbs[] = l($edit->name, FOLDER_PATH .'/'. $edit->fid);
  }
  drupal_set_breadcrumb($breadcrumbs);

  return _mmedia_folder_form($edit);
}

/**
 *  Validate the media folder
 */
function mmedia_folder_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    drupal_goto(FOLDER_PATH .'/'. $values['fid'] .'/delete');
    exit();
  }
  _mmedia_folder_process($values);
}

/**
 *  Submission of the folder edit form
 */
function mmedia_folder_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  $folder = _mmedia_folder_post($values);
  if ($folder->fid) {
    $form_state['fid'] = $folder->fid;
    $form_state['redirect'] = FOLDER_PATH .'/'. $folder->fid;
  }
}

/**
 *  Delete folder form.
 */
function mmedia_folder_delete_form(&$form_state, $folder) {
  $breadcrumbs = _mmedia_set_breadcrumb($folder);
  array_pop($breadcrumbs);
  drupal_set_breadcrumb($breadcrumbs);

  // remember for confirmation of the folder
  $form['parent'] = array('#type' => 'value', '#value' => $folder->parent);
  $form['fid'] = array('#type' => 'value', '#value' => $folder->fid);
  if (user_access('delete media')) {
    $form['delete'] = array('#type' => 'checkbox', '#title' => t('Delete contained media?'));
  }

  // provide user interaction
  $notice = t('This action cannot be undone.');
  $notice .= ' '. t('It will delete all sub-folders.');

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $folder->name)),
    isset($_GET['destination']) ? $_GET['destination'] : FOLDER_PATH .'/'. $folder->fid,
    $notice,
    t('Delete'), t('Cancel'));

  return $form;
}

/**
 *  Submission for folder delete
 */
function mmedia_folder_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['confirm'] == 1) {
    $fid = $values['fid'];
    $delete = isset($values['delete']) && $values['delete'];
    mmedia_folder_delete($fid, $delete);
  }
  $form_state['redirect'] = FOLDER_PATH . ($values['parent'] ? '/'. $values['parent'] : '');
}

/**
 *  Move folder form
 */
function mmedia_folder_move_form(&$form_state, $folder) {
  drupal_add_css(drupal_get_path('module', 'mmedia') .'/mmedia.css');

  $breadcrumbs = _mmedia_set_breadcrumb($folder);
  array_pop($breadcrumbs);
  drupal_set_breadcrumb($breadcrumbs);

  $folders = _mmedia_folder_cache();

  // add everything else
  $base = $folder->path;
  $current = 0;
  $lines = $folders['line'];
  foreach ($lines as $line => $parent) {
    // ignore all of those which are either this folder or sub-folders
    if (strpos($line, $base) === 0) {
      continue;
    }

    $depth = (count(explode('/', $line)) - 2);

    // add the appropriate boundaries for accordian viewing
    $prefix = '';
    while ($depth > $current++) {
      $prefix .= '<div class="folder-level">';
    }
    while ($current > $depth + 1) {
      $prefix .= '</div>';
      $current--;
    }

    //
    $form['radios'][$line] = array(
      '#prefix' => $prefix .'<div class="folder-level-'. $depth .'">',
      '#type' => 'radio',
      '#title' => $folders[$parent]['name'],
      '#return_value' => $parent,
      '#default_value' => ($parent == $folder->parent ? $parent : FALSE),
      '#parents' => array('folder'),
      '#suffix' => '</div>'
    );
    $current = $depth;
  }
  $form['radios']['#suffix'] = '';
  while ($current > 0) {
    $form['radios']['#suffix'] .= '</div>';
    $current--;
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));
  $form['fid'] = array('#type' => 'value', '#value' => $folder->fid);

  return $form;
}

/**
 *  Submission of the move folder.
 */
function mmedia_folder_move_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Submit')) {
    mmedia_folder_move($values['fid'], $values['folder']);
  }
}