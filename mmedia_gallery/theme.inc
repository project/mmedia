<?php

/**
 * Defines galleries for use through-out the site.
 */
function mmedia_gallery_theme_render($theme, $options, $items) {
  // provides a theming process for rendering items
  return mmedia_gallery_theme_invoke('render', $theme, $options, $items);

}

/**
 * List the themes that currently are available for media items
 */
function mmedia_gallery_theme_list() {
  return mmedia_gallery_theme_invoke('list', null, array(), '');
}

/**
 * Allows for a theme to be configured.
 */
function mmedia_gallery_theme_configure($theme, $edit = array(), $parent = '') {
  return mmedia_gallery_theme_invoke('configure', $theme, $edit, $parent);
}

/**
 * Gallery themes validation process.
 */
function mmedia_gallery_theme_validate($theme, $edit = array(), $parent = '') {
  mmedia_gallery_theme_invoke('validate', $theme, $edit, $parent);
}

/**
 * Invoke the handler for the gallery theming
 */
function mmedia_gallery_theme_invoke($op, $theme, $details, $extra) {
  static $list;

  // create the gallery list
  if (!isset($list)) {
    $list = array();
    foreach (module_list() as $module) {
      $func = $module .'_gallery';
      if (function_exists($func)) {
        if (is_array($values = $func('list', null, array(), ''))) {
          foreach ($values as $item) {
            $list[$item] = $module;
          }
        }
      }
    }
  }

  switch ($op) {
    case 'list':
      return array_keys($list);
      break;
    default:
      if (array_key_exists($theme, $list)) {
        $func = $list[$theme] .'_gallery';
        return $func($op, $theme, $details, $extra);
      }
      break;
  }
}