<?php

/**
 * @file
 *
 * Defines the menu structure for mmedia_gallery.
 *
 */

// load from database all the galleries that have a URL path
$items = array();
$results = db_query("SELECT id, menu_path, menu_details, page_rss FROM {media_gallery} WHERE menu_path != '' AND page_visible = 1");
while ($object = db_fetch_object($results)) {
  $details = unserialize($object->menu_details);
  $page = media_gallery_page_load($object->id);
  $items[$object->menu_path] = array(
    'title' => $details['title'] ? $details['title'] : t('Gallery'),
    'page callback' => 'mmedia_gallery_display_page',
    'page arguments' => array($page, $object->menu_path),
    'access callback' => '_mmedia_gallery_check',
    'access arguments' => array('view', $page),
    'file' => 'pages.inc',
    'type' => $details['type'] ? $details['type'] : MENU_NORMAL_ITEM,
  );
  if ($object->page_rss) {
    $items[$object->menu_path .'/feed'] = array(
      'title callback' => '_mmedia_gallery_page_title',
      'title arguments' => array($page),
      'page callback' => 'mmedia_gallery_display_feed',
      'page arguments' => array($page),
      'access callback' => '_mmedia_gallery_check',
      'access arguments' => array('view', $page),
      'file' => 'pages.inc',
      'type' => MENU_CALLBACK,
    );
  }
}

// allow for some way to access all the different galleries
$items['gallery/%media_gallery_page'] = array(
  'title callback' => '_mmedia_gallery_page_title',
  'title arguments' => array(1),
  'page callback' => 'mmedia_gallery_display_page',
  'page arguments' => array(1, ''),
  'access callback' => '_mmedia_gallery_check',
  'access arguments' => array('view', 1),
  'file' => 'pages.inc',
  'type' => MENU_NORMAL_ITEM,
);

// allow for some way to access all the different galleries
$items['gallery/%media_gallery_page/feed'] = array(
  'title callback' => '_mmedia_gallery_page_title',
  'title arguments' => array(1),
  'page callback' => 'mmedia_gallery_display_feed',
  'page arguments' => array(1),
  'access callback' => '_mmedia_gallery_check',
  'access arguments' => array('view', 1),
  'file' => 'pages.inc',
  'type' => MENU_CALLBACK,
);
