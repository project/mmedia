<?php
$count = 1;
?>
<?php if (count($rows)): ?>
<table id="sort<?php print $form['#id'] ?>">
  <thead>
    <th><?php print t('Field') ?></th>
    <th><?php print t('Operation') ?></th>
    <th><?php print t('Value') ?></th>
    <th></th>
  </thead>
  <tbody>
    <?php foreach ($rows as $key => $row) { ?>
    <tr class="<?php print ($count++ % 2 == 0 ? "even" : "odd"); ?>">
      <td class="attached"><?php print $row['field'] ?></td>
      <td><?php print $row['op'] ?></td>
      <td><?php print $row['value'] ?></td>
      <td><?php print $row['complete'] ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<?php endif; ?>
<?php print $form_complete; ?>