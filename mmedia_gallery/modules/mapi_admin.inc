<?php

/**
 * Implementation of hook_mapi_admin().
 */
function mmedia_gallery_mapi_admin($op) {
  switch ($op) {
    case 'menu':
      $items['admin/mapi/gallery'] = array(
        'title' => 'Gallery',
        'description' => 'Provides media galleries, essentially lists of media themed according to display preference.',
        'page callback' => 'mmedia_gallery_admin_page',
        'page arguments' => array(),
        'file' => 'admin.inc',
      );

      $items['admin/mapi/gallery/list'] = array(
        'title' => 'List',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );

      $items['admin/mapi/gallery/add'] = array(
        'title' => 'Add',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mmedia_gallery_edit_form', null),
        'type' => MENU_LOCAL_TASK,
        'file' => 'admin.inc',
      );

      $items['admin/mapi/gallery/%media_gallery'] = array(
        'title callback' => '_mmedia_gallery_title',
        'title arguments' => array(3),
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mmedia_gallery_edit_form', 3),
        'file' => 'admin.inc',
      );

      $items['admin/mapi/gallery/%media_gallery/edit'] = array(
        'title' => 'Edit',
        'type' => MENU_DEFAULT_LOCAL_TASK,
      );

      $items['admin/mapi/gallery/%media_gallery_block/block'] = array(
        'title' => 'Block',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mmedia_gallery_block_form', 3),
        'type' => MENU_LOCAL_TASK,
        'file' => 'admin.inc',
      );

      $items['admin/mapi/gallery/%media_gallery_page/page'] = array(
        'title' => 'Page',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mmedia_gallery_page_form', 3),
        'type' => MENU_LOCAL_TASK,
        'file' => 'admin.inc',
      );

      $items['admin/mapi/gallery/%media_gallery/delete'] = array(
        'title' => 'Delete',
        'page callback' => 'drupal_get_form',
        'page arguments' => array('mmedia_gallery_delete_form', 3),
        'type' => MENU_CALLBACK,
        'file' => 'admin.inc',
      );

      return $items;
  }
}

/**
 *  Helper functions for menu titles
 */
function _mmedia_gallery_title($gallery) {
  return $gallery->name;
}

/**
 *  Gets the base derivative breadcrumb
 */
function _mmedia_gallery_breadcrumb($gallery) {
  $list = array(
    l(t('Home'), NULL),
    l(t('Administer'), 'admin'),
    l(t('Media management'), 'admin/mapi'),
    l(t('Gallery'), 'admin/mapi/gallery'),
  );

  if ($gallery) {
    $list[] = l($gallery->name, 'admin/mapi/gallery/'. $gallery->id);
  }

  return $list;
}
