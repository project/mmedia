<?php

/**
 * Minimal loading for the gallery
 */
function media_gallery_load($id, $refresh = false) {
  static $cache = array();

  if (!is_numeric($id)) {
    return FALSE;
  }

  // if gallery cached
  if (!array_key_exists($id, $cache) || $refresh) {
    // load from database
    $object = db_fetch_object(db_query("SELECT id, name, filter, sort, page_visible, page_theme, block_visible, block_theme FROM {media_gallery} WHERE id = %d", $id));

    // unserialize those that are necessary.
    if ($object) {
      $object->sort   = unserialize($object->sort);
      $object->filter = unserialize($object->filter);
    }

    $cache[$id] = $object;
  }

  return $cache[$id];
}

/**
 * Page loading for the gallery
 */
function media_gallery_page_load($id, $refresh = false) {
  static $cache = array();

  if (!is_numeric($id)) {
    return FALSE;
  }

  // if gallery cached
  if (!array_key_exists($id, $cache) || $refresh) {
    // load from database
    $object = db_fetch_object(db_query("SELECT id, name, filter, sort, page_visible, page_title, page_theme, page_rss, page_paging, page_details, page_access, page_count, menu_path FROM {media_gallery} WHERE id = %d AND page_visible = 1", $id));

    // unserialize those that are necessary.
    if ($object) {
      $object->sort         = unserialize($object->sort);
      $object->filter       = unserialize($object->filter);
      $object->page_access  = unserialize($object->page_access);
      $object->page_details = unserialize($object->page_details);
    }

    $cache[$id] = $object;
  }

  return $cache[$id];
}

/**
 * Minimal loading for the gallery
 */
function media_gallery_block_load($id, $refresh = false) {
  static $cache = array();

  if (!is_numeric($id)) {
    return FALSE;
  }

  // if gallery cached
  if (!array_key_exists($id, $cache) || $refresh) {
    // load from database
    $object = db_fetch_object(db_query("SELECT id, name, filter, sort, block_visible, block_title, block_theme, block_more, block_url, block_details, block_access, block_count FROM {media_gallery} WHERE id = %d AND block_visible = 1", $id));

    // unserialize those that are necessary.
    if ($object) {
      $object->sort          = unserialize($object->sort);
      $object->filter        = unserialize($object->filter);
      $object->block_access  = unserialize($object->block_access);
      $object->block_details = unserialize($object->block_details);
    }

    $cache[$id] = $object;
  }

  return $cache[$id];
}
/**
 * List of all the galleries on the system.
 */
function media_gallery_list($refresh = false) {
  static $list;

  if (!isset($list) || $refresh) {
    $list = array();
    $results = db_query("SELECT id, name FROM {media_gallery} ORDER BY name ASC");
    while ($object = db_fetch_object($results)) {
      $list[$object->id] = $object->name;
    }
  }
  return $list;
}

/**
 * Returns list of operations that are available for the find.
 */
function media_gallery_operations() {
  return array('=', '!=', '>', '>=', '<', '<=', 'contains');
}


/**
 * Invokes the hook_gallery_fields().
 */
function media_gallery_field_invoke($op, $table = null, $field = null, $value = null) {
  static $cache;

  // cache which module handles which table.
  if (!isset($cache)) {
    $cache = array();
    foreach (module_list() as $module) {
      $func = $module .'_gallery_fields';
      if (function_exists($func) && ($result = $func('list', null, null, null))) {
        foreach ($result as $table) {
          $cache[$table] = $module;
        }
      }
    }
  }

  switch ($op) {
    case 'list':
      return array_keys($cache);
    default:
      if (array_key_exists($table, $cache)) {
        $func = $cache[$table] .'_gallery_fields';
        return $func($op, $table, $field, $value);
      }
  }
}
/**
 * Returns the fields available for the table.
 * Array values are in the format: key is field name, value is description.
 */
function media_gallery_fields($table) {
  if (is_array($result = media_gallery_field_invoke('fields', $table, null, null))) {
    return $result;
  }
  return array();
}

/**
 * A list of the tables that are available for use.
 */
function media_gallery_tables() {
  $list = array();
  $tables = media_gallery_field_invoke('list');
  foreach ($tables as $table) {
    $desc = media_gallery_field_invoke('description', $table);
    $desc = $desc ? $desc : $table;
    $list[$table] = $desc;
  }
  return $list;
}


/**
 * Gets usable list of fields for form options.
 */
function media_gallery_field_list() {
  $fields = array();
  foreach (media_gallery_tables() as $table => $table_name) {
    foreach (media_gallery_fields($table) as $field => $field_name) {
      $fields[$table .'.'. $field] = $field_name;
    }
  }
  // alphabetically sort the names.
  asort($fields);
  return $fields;
}

/**
 * Handles the details for the form
 */
function media_gallery_field($op, $table, $field, $value) {
  switch ($op) {
    // creates a configure form for the field.
    case 'configure':
      if (is_array($result = media_gallery_field_invoke('configure', $table, $field, $value))) {
        return $result;
      }
      return array('#type' => 'textfield', '#size' => 10, '#default_value' => $value);
      break;

    // validates the data within the field.
    case 'validate':
      return media_gallery_field_invoke($op, $table, $field, $value);
      break;

    // provides an actual value to use for the field in the SQL statement.
    case 'value':
      // handles the case when value doesn't return anything. null is not an option to return
      $result = media_gallery_field_invoke($op, $table, $field, $value);
      return is_null($result) ? $value : $result;
      break;
  }
}

/**
 * Helper function to validate that the filtering fields exist
 */
function _media_gallery_search_validate($item, &$joins, $ops, &$types) {
  static $reference = 1;

  $key = isset($item['key']) && is_numeric($item['key']) ? $item['key'] : 0;
  $table = isset($item['table']) && is_string($item['table']) ? $item['table'] : 'media';
  $schema = drupal_get_schema($table);
  $fields = $schema['fields'];
  if (!$schema || !$fields || !isset($item['field']) || !array_key_exists($item['field'], $fields)) {
    return false;
  }

  // check for operations to occur
  if ($ops == 'where' && (!isset($item['op']) || !isset($item['value']) || !in_array($item['op'], media_gallery_operations()))) {
    return false;
  }
  // or that the sort direction is correct
  elseif ($ops == 'sort' && isset($item['dir']) && !in_array(drupal_strtolower($item['dir']), array('asc', 'desc'))) {
    return false;
  }

  // make sure that a reference to the table exists
  if (!isset($joins[$table][$key])) {
    $joins[$table][$key] = 'r'. $reference++;
  }

  // return table, field, op and value
  $return = array('table' => $table, 'field' => $item['field'], 'key' => $key);
  if ($ops == 'where') {
    $return['op'] = $item['op'];
    // convert field value into whatever is required for the field.
    $return['value'] = media_gallery_field('value', $table, $item['field'], $item['value']);
  }
  // dealing with the sort algorithm
  if ($ops == 'sort' && $item['dir']) {
    $return['dir'] = drupal_strtolower($item['dir']);
  }

  $types = $fields[$item['field']]['type'];

  return $return;
}

/**
 * Single pass generation of search SQL statement
 */
function media_gallery_search_sql($where, $sort, &$values) {
  // construct what is wanted
  $joins = array('media' => array(0 => 'm'));

  // check where statement
  $values = array();
  $sql_where = array();
  if (is_array($where)) {
    foreach ($where as $field) {
      // handle a correct field
      if ($field = _media_gallery_search_validate($field, $joins, 'where', $type)) {
        $place = db_type_placeholder($type);
        // determine operation
        switch (drupal_strtolower($field['op'])) {
          case '=': case '=>': case '<=':
          case '>': case '<':     $op = drupal_strtoupper($field['op']); break;
          case '<>': case '!=':   $op = '<>'; break;
          case 'contains':        $field['value'] = '%%'. $field['value'] .'%%';
          case '~':  case 'like': $op = 'LIKE'; break;
          default: case '==':     $op = '='; break;
        }
        $table = $field['table'];

        // add to where listing
        $sql_where[] = $joins[$table][$field['key']] .'.'. $field['field'] ." $op ". $place;
        $values[] = media_gallery_field('value', $table, $field['field'], $field['value']);
      }
    }
  }

  // check sort statement
  $sql_sort = array();
  if (is_array($sort)) {
    foreach ($sort as $field) {
      if ($field = _media_gallery_search_validate($field, $joins, 'sort', $type)) {
        $direction = isset($field['dir']) && drupal_strtolower($field['dir']) == 'desc' ? 'DESC' : 'ASC';
        $table = $field['table'];

        // add field to output table
        $sql_sort[] = $joins[$table][$field['key']] .'.'. $field['field'] .' '. $direction;
      }
    }
  }

  // now really construct SQL, we don't do the search, just provide the SQL for it.
  $sql = 'SELECT DISTINCT (m.mid) FROM {media} m ';
  // handle joins, minus the already existing node table
  unset($joins['media']);
  foreach ($joins as $table => $references) {
    foreach ($references as $reference) {
      $sql .= ' INNER JOIN {'. $table ."} $reference ON $reference.mid = m.mid";
    }
  }

  // handle where statements
  if (count($sql_where)) {
    $sql .= ' WHERE '. implode(' AND ', $sql_where);
  }

  // handle the sorts
  if (count($sql_sort)) {
    $sql .= ' ORDER BY '. implode(', ', $sql_sort);
  }

  return $sql;
}

/**
 * Do a search of the media tables, based on the general details.
 */
function media_gallery_search($where, $sort, $options = array()) {
  $start = isset($options['start']) && is_numeric($options['start']) ? $options['start'] : 0;
  $count = isset($options['count']) && is_numeric($options['count']) ? $options['count'] : 10;

  $sql = media_gallery_search_sql($where, $sort, $values);

  // get the sequence, and return all the things found
  $list = array();
  $results = db_query_range($sql, $values, $from, $count);
  while ($object = db_fetch_object($results)) {
    $list[] = $object->mid;
  }

  return $list;
}

