<?php

$path = drupal_get_path('module', 'mmedia_gallery');
require_once($path .'/gallery.inc');
require_once($path .'/theme.inc');

/**
 * Implementation of hook_init().
 */
function mmedia_gallery_init() {
  // Load module support additions
  $files = drupal_system_listing('.*\.inc$', drupal_get_path('module', 'mmedia_gallery') .'/modules', 'name', 0);
  foreach ($files as $module => $file) {
    if (module_exists($module)) {
      require_once($file->filename);
    }
  }
}

/**
 * Implementation of hook_theme().
 */
function mmedia_gallery_theme() {
  $items = array(
    'mmedia_gallery_filter' => array(
      'template' => 'admin-media-gallery-filter',
      'arguments' => array('form' => NULL),
      'file' => 'admin.inc',
    ),
    'mmedia_gallery_sort' => array(
      'template' => 'admin-media-gallery-sort',
      'arguments' => array('form' => NULL),
      'file' => 'admin.inc',
    ),
    'mmedia_gallery_table' => array(
      'template' => 'admin-media-gallery-table',
      'arguments' => array('form' => NULL),
      'file' => 'admin.inc',
    ),
  );

  // handle distinct theme definitions for override
  foreach (mmedia_gallery_theme_list() as $theme) {
    $theme_items = mmedia_gallery_theme_invoke('theme', $theme, null, null);
    if (is_array($theme_items)) {
      $items = array_merge($items, $theme_items);
    }
  }

  return $items;
}

/**
 * Implementation of hook_menu().
 */
function mmedia_gallery_menu() {
  include(drupal_get_path('module', 'mmedia_gallery') .'/menu.inc');
  return $items;
}

/**
 * Implementation of hook_block().
 */
function mmedia_gallery_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $result = db_query("SELECT id, name, block_title FROM {media_gallery} WHERE block_theme <> ''");
      while ($object = db_fetch_object($result)) {
        $blocks[$object->id]['info'] = t('Media Gallery') .': '. check_plain($object->block_title ? $object->block_title : $object->name);
      }
      return $blocks;
    case 'view':
      // display block
      if ($gallery = media_gallery_block_load($delta)) { 
        if (_mmedia_gallery_check('view', $gallery)) {
          $items = media_gallery_search($gallery->filter, $gallery->sort, array('count' => $gallery->block_count ? $gallery->block_count : 3));
          if ($content = mmedia_gallery_theme_render($gallery->block_theme, $gallery->block_details, $items)) {
            $block['subject'] = $gallery->block_title ? $gallery->block_title : $gallery->name;
            $block['content'] = $content;
            if ($gallery->block_more) {
              // load the correct URL for the block
              $url = $gallery->block_url ? $gallery->block_url : 'gallery/'. $gallery->id;
              $block['content'] .= theme('more_link', url($url), $gallery->block_title ? $gallery->block_title : $gallery->name);
            }
          }
        }
      }
      return $block;
      break;
  }
}

/**
 * Implementation of hook_gallery().
 */
function mmedia_gallery_gallery($op, $theme, $data, $extra) {
  switch ($op) {
    case 'list':
      $files = drupal_system_listing('.*\.inc$', drupal_get_path('module', 'mmedia_gallery') .'/gallery', 'name', 0);
      return array_keys($files);
    case 'configure':
      include drupal_get_path('module', 'mmedia_gallery') .'/gallery/'. $theme .'.inc';
      return $form;
      break;
    case 'validate':
      include drupal_get_path('module', 'mmedia_gallery') .'/gallery/'. $theme .'.inc';
      break;
    case 'render':
      include drupal_get_path('module', 'mmedia_gallery') .'/gallery/'. $theme .'.inc';
      return $output;
      break;
    case 'theme':
      include drupal_get_path('module', 'mmedia_gallery') .'/gallery/'. $theme .'.inc';
      return $items;
      break;
  }
}

/**
 * Implementation of hook_gallery_fields().
 */
function mmedia_gallery_fields($op, $table, $field, $value) {
  $include = drupal_get_path('module', 'mmedia_gallery') .'/gallery_fields.inc';
  switch ($op) {
    case 'list':
      require $include;
      return array_keys($tables);
    case 'fields':
      require $include;
      return $tables[$table];
      break;
    case 'configure':
      require $include;
      return $form;
      break;
    case 'validate':
      require $include;
      break;
    case 'value':
      require $include;
      return $value;
      break;
  }
}

/**
 * Access check for the galleries.
 */
function _mmedia_gallery_check($op, $gallery) {
  global $user;
  
  // check the page access section
  if ($gallery->page_access) {
    // check to see if there are any roles which match both user and access.
    if (count($gallery->page_access['roles']) && !count(array_intersect_key($gallery->page_access['roles'], $user->roles))) {
      return false;
    }
  }
  // check the block access section
  elseif ($gallery->block_access) {
    // check to see if there are any roles which match both user and access.
    if (count($gallery->block_access['roles']) && !count(array_intersect_key($gallery->block_access['roles'], $user->roles))) {
      return false;
    }
    // check if the current node (if displaying only the node)
    if (count($gallery->block_access['nodes'])) {
      if (preg_match('/^node\/(\d+)$/', $_GET['q'], $match)) {
        $node = node_load($match[1]);
        if (array_key_exists($node->type, $gallery->block_access['nodes'])) {
          return true;
        }
      }
      return false;
    }
  }
  
  return true;
}

/**
 * Gallery Page Title.
 */
function _mmedia_gallery_page_title($gallery) {
  return !empty($gallery->page_title) ? $gallery->page_title : $gallery->name;
}
