<?php

$tables = array(
  'media' => array('uid', 'name', 'ext', 'title', 'summary', 'expire', 'source', 'reference', 'created', 'changed'),
  'media_extras' => array('licence', 'description', 'bitrate', 'quality', 'length'),
  'media_metadata' => array('type', 'value'),
);

switch ($op) {
  case 'configure':
    switch ($field) {
      case 'type':
        $form = array('#type' => 'select', '#options' => drupal_map_assoc(mapi_type_list()), '#default_value' => $value);
        break;
      case 'uid':
        $form = array('#type' => 'textfield', '#autocomplete_path' => 'user/autocomplete', '#default_value' => $value);
        break;
    }
    break;
  case 'validate':
    break;
  case 'value':
    switch ($field) {
      case 'changed':
      case 'created':
        $value = !is_numeric($value) ? strtotime($value) : $value;
        break;
    }
    break;
}

