
The main concept here is to provide a method for creating themed galleries

Since all galleries require something of the database, we use the fields provided by the system
to go into the elements the theme wants.
Furthermore, we also provide extra "fields" which represent extra things that may need to be used. i.e. links, filenames,
derivatives, etc. This will create a VERY long list, but it should be worth it.

This applies for each different theme.


