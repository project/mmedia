<?php

/**
 * Display a page of themed galleries
 */
function mmedia_gallery_display_page($gallery, $extra = '') {
  // TODO: add the selection of extra filters, based on the extra passed through (i.e. menu_url)

  // generate the necessary items, based on whether it is paging....
  $count = $gallery->page_count ? $gallery->page_count : 10;
  
  // do the search for the items
  $sql = media_gallery_search_sql($gallery->filter, $gallery->sort, $values);

  $results = pager_query($sql, $count, 0, NULL, $values);
  $items = array();
  while ($object = db_fetch_object($results)) {
    $items[] = (object)array(
      'filename' => media_filename(media_load($object->mid)),
      'media' => media_load($object->mid),
      'metadata' => media_metadata_load($object->mid),
    );
  }
  
  // render the output of the theme
  $output = mmedia_gallery_theme_render($gallery->page_theme, $gallery->page_details, $items);

  // add pager
  if ($gallery->page_paging) {
    $output .= theme('pager', NULL, $count);
  }

  // add RSS feed indicator
  if ($gallery->page_rss) {
    $path = ($gallery->menu_path ? $gallery->menu_path : 'gallery/'. $gallery->id);
    drupal_add_feed(url($path .'/feed'), _mmedia_gallery_page_title($gallery));
  }

  // show that rendered output
  return $output;
}

/**
 * Provide the feed given the gallery list.
 */
function mmedia_gallery_display_feed($gallery) {
  // Only if RSS enabled is this displayed
  if ($gallery->page_rss) {
    require_once(drupal_get_path('module', 'mmedia') .'/media.pages.inc');
    
    $items = media_gallery_search($gallery->filter, $gallery->sort, array('count' => $gallery->page_count ? $gallery->page_count : 10));
    mmedia_feed($items);  
  }
  // otherwise redirect to not found.
  else {
    drupal_not_found();
  }
  exit;  
}
