<?php
$profile = $details['profile'] ? array('profile' => $details['profile']) : array('profile' => null);
?>
<?php foreach ($items as $media) { ?>
  <div class="media">
    <?php print mapi_display($media->filename, $profile); ?>
  </div>
<?php } ?>
