<?php
$profile = $details['profile'] ? array('profile' => $details['profile']) : array('profile' => null);
$width = 100 / ($details['item_count'] ? $details['item_count'] : 1);
?>
<div class="media-group">
<?php foreach ($items as $media) { ?>
  <div class="media" style="float: left; width: <?php print $width; ?>%;">
    <?php print mapi_display($media->filename, $profile); ?>
  </div>
<?php } ?>
</div>
<div class="clear-block></div>