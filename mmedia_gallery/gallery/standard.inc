<?php

switch ($op) {
  case 'configure':
    $form = array();
    $form['profile'] = array(
      '#type' => 'select',
      '#title' => t('Profile'),
      '#description' => t('Profile to display for each media file.'),
      '#options' => array_merge(array('' => t('none')), drupal_map_assoc(mapi_profile_list())),
      '#default_value' => @$data['profile'],
    );
    break;
  case 'validate':
    break;
  case 'render':
    $output = theme('media_gallery_standard', $data, $extra);
    break;
  case 'theme':
    $items['media_gallery_standard'] = array(
      'template' => 'media-gallery-standard',
      'arguments' => array('details' => NULL, 'items' => array()),
      'path' => drupal_get_path('module', 'mmedia_gallery') .'/gallery',
      'file' => 'standard.inc',
    );
    break;
}
