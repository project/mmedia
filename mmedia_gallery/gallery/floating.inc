<?php

switch ($op) {
  case 'configure':
    $form = array();
    $form['profile'] = array(
      '#type' => 'select',
      '#title' => t('Profile'),
      '#description' => t('Profile to display for each media file.'),
      '#options' => array_merge(array('' => t('none')), drupal_map_assoc(mapi_profile_list())),
      '#default_value' => @$data['profile'],
    );
    $form['item_count'] = array(
      '#type' => 'select',
      '#title' => t('Item Count'),
      '#description' => t('This is the number of items per "line".'),
      '#options' => drupal_map_assoc(range(1, 10)),
      '#default_value' => @$data['item_count'],
    );
    break;
  case 'validate':
    break;
  case 'render':
    $output = theme('media_gallery_floating', $data, $extra);
    break;
  case 'theme':
    $items['media_gallery_floating'] = array(
      'template' => 'media-gallery-floating',
      'arguments' => array('details' => NULL, 'items' => array()),
      'path' => drupal_get_path('module', 'mmedia_gallery') .'/gallery',
      'file' => 'floating.inc',
    );
    break;
}
