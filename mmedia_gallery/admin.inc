<?php

/**
 *
 */
function mmedia_gallery_admin_page() {
  // list the entire set of galleries available.
  $list = media_gallery_list();
  $rows = array();
  foreach ($list as $id => $title) {
    $rows[] = array(
      l($title, 'admin/mapi/gallery/'. $id),
    );
  }

  return theme('table', array(''), $rows);
}

/**
 * Main definition of the gallery.
 */
function mmedia_gallery_edit_form($form_state, $gallery) {
  if (isset($form_state['storage']['gallery'])) {
    $gallery = $form_state['storage']['gallery'];
    if ($gallery->id) {
      drupal_set_message(t('Changes have been made. To save click on Update.'), 'warning');
    }
  }
  $breadcrumbs = _mmedia_gallery_breadcrumb($gallery);
  drupal_set_breadcrumb($breadcrumbs);

  drupal_set_title(!$gallery->id ? t('Add Gallery') : t('Edit %gallery', array('%gallery' => $gallery->name)));

  // this is what edits the gallery
  $form['id'] = array('#type' => 'value', '#value' => $gallery->id);

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('The name of the gallery, must only include alphanumeric, hyphen, underscore or space characters.'),
    '#default_value' => $gallery->name,
  );

  $theme_list = drupal_map_assoc(mmedia_gallery_theme_list());

  $form['page_theme'] = array(
    '#type' => 'select',
    '#title' => t('Page Display'),
    '#description' => t('Select the theme for the page display.'),
    '#options' => array_merge(array('' => t('Not shown')), $theme_list),
    '#default_value' => $gallery->page_theme,
  );

  $form['block_theme'] = array(
    '#type' => 'select',
    '#title' => t('Block Display'),
    '#description' => t('Select the theme for the block display.'),
    '#options' => array_merge(array('' => t('Not shown')), $theme_list),
    '#default_value' => $gallery->block_theme,
  );

  $metadata_list = drupal_map_assoc(media_gallery_field_list());

  // now handle the filtering process
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );
  $form['filter']['details'] = array(
    '#theme' => 'mmedia_gallery_filter',
  );
  $filter_ops = drupal_map_assoc(media_gallery_operations());
  if (!empty($gallery->filter) && is_array($gallery->filter)) {
    foreach ($gallery->filter as $key => $item) {
      // get field details
      $field = explode('.', $item['field']);
      $table = count($field) == 2 ? $field[0] : 'media';
      $field = count($field) == 2 ? $field[1] : $field[0];

      // get configuration details, default to textfield if nothing specific exists
      $value = media_gallery_field('configure', $table, $field, $item['value']);
      $value = is_array($value) ? $value : array('#type' => 'textfield', '#default_value' => $item['value']);
      $form['filter']['details'][$key] = array(
        'field' => array('#type' => 'hidden', '#value' => $item['field'], '#suffix' => check_plain($field)),
        'op' => array('#type' => 'select', '#default_value' => $item['op'], '#options' => $filter_ops),
        'value' => $value,
        'delete' => array('#type' => 'submit', '#name' => 'delete_filter_'. $key, '#value' => t('Remove'), '#parents' => array('delete_filter', $key)),
      );
    }
  }
  else {
    $form['filter']['message'] = array('#value' => '<div>'. t('Nothing current selected.') .'</div>');
  }
  $form['filter']['add'] = array(
    '#theme' => 'mmedia_gallery_table',
    0 => array(
      'field' => array('#type' => 'select', '#options' => $metadata_list),
      'button' => array('#type' => 'submit', '#value' => t('Add Filter')),
    ),
  );

  // and then the sorting process
  $form['sort'] = array(
    '#type' => 'fieldset',
    '#title' => t('Sort'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );
  $form['sort']['details'] = array(
    '#theme' => 'mmedia_gallery_sort',
  );
  if (!empty($gallery->sort) && is_array($gallery->sort)) {
    foreach ($gallery->sort as $key => $item) {
      $form['sort']['details'][$key] = array(
        'field' => array('#type' => 'hidden', '#value' => $item['field'], '#suffix' => check_plain($item['field'])),
        'dir' => array('#type' => 'select', '#default_value' => $item['dir'], '#options' => array('asc' => t('Ascending'), 'desc' => t('Descending'))),
        'delete' => array('#type' => 'submit', '#name' => 'delete_sort_'. $key, '#value' => t('Remove'), '#parents' => array('delete_sort', $key)),
      );
    }
  }
  else {
    $form['sort']['message'] = array('#value' => '<div>'. t('Nothing current selected.') .'</div>');
  }
  $form['sort']['add'] = array(
    '#theme' => 'mmedia_gallery_table',
    0 => array(
      'field' => array('#type' => 'select', '#options' => $metadata_list),
      'button' => array('#type' => 'submit', '#value' => t('Add Sort')),
    ),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => $gallery->id ? t('Update') : t('Add'));
  if ($gallery->id) {
    $form['delete'] = array('#type' => 'submit', '#value' => t('Delete'));
  }

  return $form;
}

/**
 * Validation for mmedia_gallery_edit_form.
 */
function mmedia_gallery_edit_form_validate($form, &$form_state) {
  if (preg_match('/[^\w\d\_\- ]/', $form_state['values']['name'])) {
    form_set_error('name', t('Name must contain only alphanumeric, hyphen, underscore or space characters.'));
  }
}

/**
 * Submission for mmedia_gallery_edit form
 */
function mmedia_gallery_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];

  // details needed for both add and update
  $record = array(
    'id' => $values['id'],
    'name' => $values['name'],
    'page_visible' => ($values['page_theme'] != ''),
    'page_theme' => $values['page_theme'],
    'block_visible' => ($values['block_theme'] != ''),
    'block_theme' => $values['block_theme'],
    'filter' => $values['filter']['details'],
    'sort' => $values['sort']['details'],
  );

  // handle the removal of specific entries.
  if (!isset($values['op'])) {
    foreach ($values as $key => $value) {
      if ($value == t('Remove')) {
        $reference = explode('_', $key);
        if (count($reference) == 3 && $reference[0] == 'delete') {
          switch ($reference[1]) {
            case 'filter': unset($record['filter'][$reference[2]]); break;
            case 'sort':   unset($record['sort'][$reference[2]]); break;
          }
        }
      }
    }
    $form_state['storage']['gallery'] = (object)$record;
  }

  switch ($values['op']) {
    // redirect to the delete page.
    case t('Delete'):
      drupal_goto('admin/mapi/gallery/'. $values['id'] .'/delete');
      break;

    case t('Add'):
      unset($record['id']);
      if (drupal_write_record('media_gallery', $record)) {
        unset($form_state['storage']);
        $form_state['id'] = $record['id'];
        $form_state['redirect'] = 'admin/mapi/gallery/'. $record['id'];
      }
      break;

    case t('Update'):
      $record['id'] = $values['id'];
      if (drupal_write_record('media_gallery', $record, array('id'))) {
        unset($form_state['storage']);
        $form_state['redirect'] = 'admin/mapi/gallery/'. $values['id'];

        // force recaching of the media_gallery
        media_gallery_load($values['id'], true);
      }
      break;

    case t('Add Filter'):
      $record['filter'][] = array(
        'field' => $values['filter']['add'][0]['field'],
        'op' => $values['filter']['add'][0]['op'],
        'value' => $values['filter']['add'][0]['value'],
      );
      $form_state['storage']['gallery'] = (object)$record;
      break;

    case t('Add Sort'):
      $record['sort'][] = array(
        'field' => $values['sort']['add'][0]['field'],
        'dir' => $values['sort']['add'][0]['dir'],
      );
      $form_state['storage']['gallery'] = (object)$record;
      break;
  }
}

/**
 * Saves the details for the gallery access
 */
function mmedia_gallery_access_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Save Changes')) {
    $values = array(
      'id' => $values['id'],
      'access' => array(
        'roles' => array_filter($values['roles']),
        'nodes' => array_values(array_filter($values['nodes'])),
      ),
    );
    drupal_write_record('media_gallery', $values, array('id'));
  }
}

/**
 * Block attributes for a gallery.
 */
function mmedia_gallery_block_form($form_state, $gallery) {
  // the details to display as a block
  $breadcrumbs = _mmedia_gallery_breadcrumb($gallery);
  drupal_set_breadcrumb($breadcrumbs);

  drupal_set_title(t('Block Settings for %gallery', array('%gallery' => $gallery->name)));

  // Gallery ID
  $form['id'] = array('#type' => 'value', '#value' => $gallery->id);

  $form['block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block Options'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );

  $form['block']['block_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Title'),
    '#description' => t('The title of the block displayed. Leave blank to use gallery name.'),
    '#default_value' => $gallery->block_title,
  );

  $form['block']['block_more'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show more link.'),
    '#description' => t('Provides the page as an RSS feed, located at the URL/feed.'),
    '#default_value' => $gallery->block_more,
  );

  $form['block']['block_url'] = array(
    '#type' => 'textfield',
    '#title' => t('More URL'),
    '#description' => t('Specifies which URL to use for the more link. Leaving blank directs to the associated gallery page.'),
    '#default_value' => $gallery->block_url,
  );

  $form['block']['block_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Block Count'),
    '#description' => t('The number of media to display in the block.'),
    '#default_value' => $gallery->block_count ? $gallery->block_count : 3,
  );

  $settings = mmedia_gallery_theme_configure($gallery->block_theme, $gallery->block_details, 'settings][info][');
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
    'info' => $settings,
  );

  $form['access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );

  // Role-based visibility settings
  $default_role_options = is_array($gallery->block_access['roles']) ? array_keys($gallery->block_access['roles']) : array();
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  $role_options = array();
  while ($role = db_fetch_object($result)) {
    $role_options[$role->rid] = $role->name;
  }
  $form['access']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show page for specific roles'),
    '#default_value' => $default_role_options,
    '#options' => $role_options,
    '#description' => t('Show this block only for the selected role(s). If you select no roles, the block will be visible to all users.'),
  );
  $form['access']['nodes'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Node types'),
    '#default_value' => is_array($gallery->block_access['nodes']) ? array_keys($gallery->block_access['nodes']) : array(),
    '#options' => node_get_types('names'),
    '#description' => t('Node types for which a block will be displayed. Selecting none displays it for all node types.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));

  return $form;
}

/**
 * Validation for mmedia_gallery_block_form.
 */
function mmedia_gallery_block_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Save Changes')) {
    if ($values['block']['block_url'] && !valid_url($values['block']['block_url'], true) && !valid_url($values['block']['block_url'], false)) {
      form_set_error('block][block_url', t('More URL is not a valid URL.'));
    }
    if (!is_numeric($values['block']['block_count']) || $values['block']['block_count'] < 1) {
      form_set_error('block][block_count', t('Block Count must be a positive number.'));
    }
  }
}

/**
 * Submission for mmedia_gallery_block_form.
 */
function mmedia_gallery_block_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Save Changes')) {
    // construct the correct record details
    $record = array('id' => $values['id']);
    $record = array_merge($record, $values['block']);
    $values['access']['roles'] = array_filter($values['access']['roles']);
    $values['access']['nodes'] = array_filter($values['access']['nodes']);
    $record['block_access'] = $values['access'];
    $record['block_details'] = $values['settings']['info'];

    drupal_write_record('media_gallery', $record, array('id'));
  }
}

/**
 * Settings for displaying an pgallery on a page.
 */
function mmedia_gallery_page_form($form_state, $gallery) {
  // the details for display on a page
  $breadcrumbs = _mmedia_gallery_breadcrumb($gallery);
  drupal_set_breadcrumb($breadcrumbs);

  drupal_set_title(t('Page Settings for %gallery', array('%gallery' => $gallery->name)));

  // Gallery ID
  $form['id'] = array('#type' => 'value', '#value' => $gallery->id);

  $form['page'] = array(
    '#type' => 'fieldset',
    '#title' => t('Page Options'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );

  $form['page']['page_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Title'),
    '#description' => t('The title of the page displayed. Leave blank to use gallery name.'),
    '#default_value' => $gallery->page_title,
  );

  $form['page']['page_rss'] = array(
    '#type' => 'checkbox',
    '#title' => t('RSS'),
    '#description' => t('Provides the page as an RSS feed, located at the URL/feed.'),
    '#default_value' => $gallery->page_rss,
  );

  $form['page']['page_paging'] = array(
    '#type' => 'checkbox',
    '#title' => t('Paging'),
    '#description' => t('Provides a paging capability for media lists longer than a page.'),
    '#default_value' => $gallery->page_paging,
  );

  $form['page']['page_count'] = array(
    '#type' => 'textfield',
    '#title' => t('Page Count'),
    '#description' => t('The number of media to display per page.'),
    '#default_value' => $gallery->page_count ? $gallery->page_count : 10,
  );

  $settings = mmedia_gallery_theme_configure($gallery->page_theme, $gallery->page_details, 'settings][info][');
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Settings'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
    'info' => $settings,
  );

  $form['menu'] = array(
    '#type' => 'fieldset',
    '#title' => t('Menu Options'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );
  $form['menu']['menu_path'] = array(
    '#type' => 'textfield',
    '#title' => t('URL Path'),
    '#description' => t('The path of the URL to be display, leave blank for no URL.'),
    '#default_value' => $gallery->menu_path,
  );

  $form['menu']['menu_details']['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Display Title'),
    '#description' => t('The title that is displayed to link within the menu.'),
    '#default_value' => $gallery->menu_details['title'],
  );

  $form['menu']['menu_details']['type'] = array(
    '#type' => 'select',
    '#title' => t('Menu Type'),
    '#description' => t('How the menu option is to be displayed within the system.'),
    '#options' => array(
      MENU_NORMAL_ITEM => t('Normal Item'),
      MENU_LOCAL_TASK => t('Local Task'),
    ),
    '#default_value' => $gallery->menu_details['type'],
  );

  $form['access'] = array(
    '#type' => 'fieldset',
    '#title' => t('Access'),
    '#collapsible' => true,
    '#collapsed' => false,
    '#tree' => true,
  );

  // Role-based visibility settings
  $default_role_options = is_array($gallery->page_access['roles']) ? array_keys($gallery->page_access['roles']) : array();
  $result = db_query('SELECT rid, name FROM {role} ORDER BY name');
  $role_options = array();
  while ($role = db_fetch_object($result)) {
    $role_options[$role->rid] = $role->name;
  }
  $form['access']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Show page for specific roles'),
    '#default_value' => $default_role_options,
    '#options' => $role_options,
    '#description' => t('Show this page only for the selected role(s). If you select no roles, the page will be visible to all users.'),
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save Changes'));

  return $form;
}

/**
 * Validation for mmedia_gallery_page_form.
 */
function mmedia_gallery_page_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Save Changes')) {
    if ($values['menu']['menu_path'] && !preg_match('/^[\w\d\-\_\%\/]+$/', $values['menu']['menu_path'])) {
      form_set_error('menu][menu_path', t('Menu path must contain only alphanumeric, hyphen, underscore, forward slash, and percent characters.'));
    }
    if (!is_numeric($values['page']['page_count']) || $values['page']['page_count'] < 1) {
      form_set_error('page][page_count', t('Page Count must be a positive number.'));
    }
  }
}

/**
 * Submission for mmedia_gallery_page_form.
 */
function mmedia_gallery_page_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Save Changes')) {
    // construct the correct record details
    $record = array('id' => $values['id']);
    $record = array_merge($record, $values['page']);
    $record = array_merge($record, $values['menu']);
    $values['access']['roles'] = array_filter($values['access']['roles']);
    $record['page_access'] = $values['access'];
    $record['page_details'] = $values['settings']['info'];

    drupal_write_record('media_gallery', $record, array('id'));
    
    // get the menu to rehash
    menu_rebuild();
  }
}

/**
 * Delete a gallery page.
 */
function mmedia_gallery_delete_form($form_state, $gallery) {
  $breadcrumbs = _mmedia_gallery_breadcrumb($gallery);
  drupal_set_breadcrumb($breadcrumbs);

  $form['gallery'] = array('#type' => 'value', '#value' => $gallery->id);

  // provide user interaction
  $notice = t('This action cannot be undone.');

  $form = confirm_form($form,
    t('Are you sure you want to delete gallery %gallery?', array('%gallery' => $gallery->name)),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/mapi/gallery/'. $gallery->id,
    $notice,
    t('Delete'), t('Cancel'));

  return $form;
}

/**
 *  Submission for mmedia_gallery_delete form
 */
function mmedia_gallery_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    db_query("DELETE FROM {media_gallery} WHERE id = %d", $values['gallery']);
    
    // potential change in menu selections.
    menu_rebuild();
  }

  $form_state['redirect'] = 'admin/mapi/gallery';
}

/**
 *
 */
function template_preprocess_mmedia_gallery_filter(&$variables) {
  $form = &$variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    $item = &$form[$key];
    $rows[] = array(
      'field' => drupal_render($item['field']),
      'op' => drupal_render($item['op']),
      'value' => drupal_render($item['value']),
      'complete' => drupal_render($item),
    );
  }
  $variables['rows'] = $rows;

  $variables['form_complete'] = drupal_render($form);
}

/**
 *
 */
function template_preprocess_mmedia_gallery_sort(&$variables) {
  $form = &$variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    $item = &$form[$key];
    $rows[] = array(
      'field' => drupal_render($item['field']),
      'dir' => drupal_render($item['dir']),
      'complete' => drupal_render($item),
    );
  }
  $variables['rows'] = $rows;

  $variables['form_complete'] = drupal_render($form);
}

/**
 *
 */
function template_preprocess_mmedia_gallery_table(&$variables) {
  $form = &$variables['form'];

  $rows = array();
  foreach (element_children($form) as $key) {
    $item = &$form[$key];
    $rows[] = array(
      'field' => drupal_render($item['field']),
      'complete' => drupal_render($item),
    );
  }
  $variables['rows'] = $rows;

  $variables['form_complete'] = drupal_render($form);
}

