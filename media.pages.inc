<?php

/**
 *  Standard content page.
 */
function mmedia_content_page() {
  $result = pager_query(db_rewrite_sql('SELECT m.mid, m.title, m.summary FROM {media} m WHERE m.public <> 0 ORDER BY m.created DESC'), variable_get('mmedia_default_main_page', 10));

  $output = '';
  $num_rows = FALSE;
  while ($media = db_fetch_object($result)) {
    $media = media_load($media->mid);
    $filename = media_filename($media);
    $profile = variable_get('mmedia_view_profile', null);
    $output .= theme('media_view', mapi_display($filename, array('options' => $profile)), $filename, $media);
    $num_rows = TRUE;
  }

  if ($num_rows) {
    $feed_url = url(MEDIA_PATH . '/rss.xml', array('absolute' => TRUE));
    drupal_add_feed($feed_url, variable_get('site_name', 'Drupal') .' '. t('RSS'));
    $output .= theme('pager', NULL, variable_get('mmedia_default_main_page', 10));
  }
  else {
    $default_message = t('<h1 class="title">Welcome to the Media display page!</h1><p>Please follow these steps to set up to start uploading and viewing media:</p>');
    $default_message .= '<ol>';

    $default_message .= '<li>'. t('<strong>Configure <a href="@presenters">presenters</a> and <a href="@profiles">profiles</a> to achieve consistant presentation of media across your website.', array('@presenters' => url('admin/mapi/presenters'), '@profiles' => url('admin/mapi/profiles'))) .'</li>';
    $default_message .= '</ol>';

    $output = '<div id="first-time">'. $default_message .'</div>';
  }
  drupal_set_title('');

  return $output;
}

/**
 *  Generic Media Feed
 */
function mmedia_feed($mids = FALSE, $channel = array()) {
  global $base_url, $language;

  if ($mids === FALSE) {
    $mids = array();
    $result = db_query_range(db_rewrite_sql('SELECT m.mid FROM {media} m WHERE m.public <> 0 ORDER BY m.created DESC'), 0, variable_get('feed_default_items', 10));
    while ($row = db_fetch_object($result)) {
      $mids[] = $row->mid;
    }
  }

  $item_length = variable_get('feed_item_length', 'teaser');
  $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

  $items = '';
  foreach ($mids as $mid) {
    // Load the specified node:
    $item = media_load($mid);
    $item->link = url(MEDIA_PATH ."/$mid", array('absolute' => TRUE));

    // Allow modules to add additional item fields and/or modify $item
    $extra = media_invoke_item('rss item', $item);
    $extra = array_merge($extra, array(array('key' => 'pubDate', 'value' => format_date($item->created, 'custom', 'r')), array('key' => 'dc:creator', 'value' => $item->name), array('key' => 'guid', 'value' => $item->nid .' at '. $base_url, 'attributes' => array('isPermaLink' => 'false'))));
    foreach ($extra as $element) {
      if (isset($element['namespace'])) {
        $namespaces = array_merge($namespaces, $element['namespace']);
      }
    }

    // Prepare the item description
    switch ($item_length) {
      case 'fulltext':
        $item_text = $item->metadata['description'];
        break;
      case 'teaser':
        $item_text = $item->summary;
        if (!empty($item->readmore)) {
          $item_text .= '<p>'. l(t('read more'), MEDIA_PATH .'/'. $item->mid, array('absolute' => TRUE, 'attributes' => array('target' => '_blank'))) .'</p>';
        }
        break;
      case 'title':
        $item_text = '';
        break;
    }

    // TODO: Allow different derivatives to be added to the RSS item.
    // add the original media file, as the primary enclosure for a RSS item.
    $filename = media_filename($item);
    if (mapi_file_exists($filename)) {
      $extra = array_merge($extra, array(array('key' => 'enclosure', 'value' => null, 'attributes' => array(
        'url' => url($filename, array('absolute' => true)),
        'length' => filesize($filename),
        'type' => $item->type,
      ))));
    }


    $items .= format_rss_item($item->title, $item->link, $item_text, $extra);
  }

  $channel_defaults = array(
    'version'     => '2.0',
    'title'       => variable_get('site_name', 'Drupal'),
    'link'        => $base_url,
    'description' => variable_get('site_mission', ''),
    'language'    => $language->language
  );
  $channel = array_merge($channel_defaults, $channel);

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss version=\"". $channel["version"] ."\" xml:base=\"". $base_url ."\" ". drupal_attributes($namespaces) .">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language']);
  $output .= "</rss>\n";

  drupal_set_header('Content-Type: application/rss+xml; charset=utf-8');
  print $output;
}

/**
 *  Handle media autocomplete.
 */
function mmedia_autocomplete($string) {
  if ($string != '') {
    if (is_numeric($string)) {
      $result = db_query_range('SELECT mid, name FROM {media} WHERE mid LIKE ("%d%%")', (int)$string, 0, 10);
    }
    else {
      $result = db_query_range('SELECT mid, name FROM {media} WHERE LOWER(name) LIKE LOWER("%%%s%%")', drupal_strtolower($string), 0, 10);
    }

    $matches = array();
    while ($object = db_fetch_object($result)) {
      $t = $object->name;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (preg_match('/,/', $t) || preg_match('/"/', $t)) {
        $t = '"'. preg_replace('/"/', '""', $t) .'"';
      }
      $matches[$object->mid] = '<span class="autocomplete_title">'. check_plain($t) .'</span>';
    }
    print drupal_to_js($matches);
    exit();
  }
}

/**
 *  Transfer the media file, including derivative.
 */
function mmedia_transfer($media, $derivative = '') {
  // get actual filename
  $filename = media_filename($media);

  // handle the derivative of a media if installed
  if (module_exists('mapi_derivatives') && in_array($derivative, mapi_derivative_list())) {
    $filename = mapi_derive($filename, $derivative);
  }

  ob_end_clean();

  // handle local file
  if (is_file($filename)) {
    if ($fd = fopen($filename, 'rb')) {
      $ext = mapi_file_ext($filename);

      // set the appropriate headers
      $headers = array(
        'Content-Type: '. ($ext ? mapi_extension_mime($ext) : 'application/octet-stream'),
        'Content-Length: '. filesize($filename),
        'Content-Disposition: attachment; filename='. mapi_file_name($filename) .'.'. $ext,
      );
      foreach ($headers as $header) {
        drupal_set_header($header);
      }

      // transfer the file in 1024 byte blocks to save on memory usage
      while (!feof($fd)) {
        print fread($fd, 1024);
      }
      fclose($fd);
    }
    // if not openable on this system.
    else {
      drupal_not_found();
    }
  }
  // handle external file
  else {
    // Temporary redirect since we want people to still come to this site for the details,
    // but allow the other site to host the media.
    header("HTTP/1.0 307 Temporary redirect");
    header("Location: ". $filename );
  }
  exit();
}

/**
 *  Folder autocomplete
 */
function mmedia_folder_autocomplete($string) {
  if ($string != '') {
    if (is_numeric($string)) {
      $result = db_query_range('SELECT fid, name, path FROM {media_folders} WHERE mid LIKE ("%d%%")', (int)$string, 0, 10);
    }
    else {
      $result = db_query_range('SELECT fid, name, path FROM {media_folders} WHERE LOWER(name) LIKE LOWER("%%%s%%")', drupal_strtolower($string), 0, 10);
    }

    $matches = array();
    while ($folder = db_fetch_object($result)) {
      $t = $folder->name;
      // Commas and quotes in terms are special cases, so encode 'em.
      if (preg_match('/,/', $folder->name) || preg_match('/"/', $folder->name)) {
        $t = '"'. preg_replace('/"/', '""', $folder->name) .'"';
      }
      $matches[$folder->fid] = '<span class="autocomplete_title">'. check_plain($t) .'</span> <span class="autocomplete_path">['. FOLDER_PATH .'/'. $folder->path .']</span>';
    }
    print drupal_to_js($matches);
    exit();
  }
}

