<?php

// media related details
$items[MEDIA_PATH] = array(
  'title' => 'Media',
  'page callback' => 'mmedia_content_page',
  'access callback' => '_mmedia_access',
  'access arguments' => array(NULL, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'media.pages.inc',
);

$items[MEDIA_PATH .'/rss.xml'] = array(
  'title' => 'RSS feed',
  'page callback' => 'mmedia_feed',
  'access callback' => '_mmedia_access',
  'access arguments' => array(NULL, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'media.pages.inc',
);

$items[MEDIA_PATH .'/%media'] = array(
  'title callback' => '_mmedia_title',
  'title arguments' => array(MEDIA_ARG + 1),
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_view_form', MEDIA_ARG + 1),
  'access callback' => '_mmedia_access',
  'access arguments' => array(MEDIA_ARG + 1, 'access'),
  'type' => MENU_NORMAL_ITEM,
  'file' => 'media.forms.inc',
);

$items[MEDIA_PATH .'/add'] = array(
  'title' => 'Create Media',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_edit_form', null),
  'access callback' => '_mmedia_access',
  'access arguments' => array(NULL, 'upload'),
  'type' => MENU_NORMAL_ITEM,
  'file' => 'media.forms.inc',
);

foreach (mapi_type_list() as $type) {
  $items[MEDIA_PATH .'/add/'. $type] = array(
    'title' => 'Create '. $type,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mmedia_edit_form', null, $type),
    'access callback' => '_mmedia_access',
    'access arguments' => array(NULL, 'upload'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'media.forms.inc',
  );
}

$items[MEDIA_PATH .'/autocomplete'] = array(
  'title' => 'autocomplete',
  'page callback' => 'mmedia_autocomplete',
  'access callback' => '_mmedia_access',
  'access arguments' => array(NULL, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'media.pages.inc',
);

$items[MEDIA_PATH .'/%media/view'] = array(
  'title' => 'View',
  'type' => MENU_DEFAULT_LOCAL_TASK,
);

$items[MEDIA_PATH .'/%media/edit'] = array(
  'title' => 'Edit',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_edit_form', MEDIA_ARG + 1),
  'access callback' => '_mmedia_access',
  'access arguments' => array(MEDIA_ARG + 1, 'manage'),
  'type' => MENU_LOCAL_TASK,
  'file' => 'media.forms.inc',
);

$items[MEDIA_PATH .'/%media/delete'] = array(
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_delete_form', MEDIA_ARG + 1),
  'access callback' => '_mmedia_access',
  'access arguments' => array(MEDIA_ARG + 1, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'media.forms.inc',
);

$items[MEDIA_PATH .'/%media/transfer'] = array(
  'page callback' => 'mmedia_transfer',
  'page arguments' => array(MEDIA_ARG + 1),
  'access callback' => '_mmedia_access',
  'access arguments' => array(MEDIA_ARG + 1, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'media.pages.inc',
);

$items[MEDIA_PATH .'/%media/move'] = array(
  'title' => 'Move',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_move_form', MEDIA_ARG + 1),
  'access callback' => '_mmedia_access',
  'access arguments' => array(MEDIA_ARG + 1, 'manage'),
  'type' => MENU_LOCAL_TASK,
  'file' => 'media.forms.inc',
);

if (module_exists('mapi_derivatives')) {
  $items[MEDIA_PATH .'/%media/derivatives'] = array(
    'title' => 'Derivatives',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mmedia_derivatives_list_form', MEDIA_ARG + 1),
    'access callback' => '_mmedia_access',
    'access arguments' => array(MEDIA_ARG + 1, 'access'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'media.derivatives.forms.inc',
  );
  $items[MEDIA_PATH .'/%media/derivatives/%mapi_derivative'] = array(
    'title' => 'Derivatives',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mmedia_derivatives_form', MEDIA_ARG + 1, MEDIA_ARG + 3),
    'access callback' => '_mmedia_access',
    'access arguments' => array(MEDIA_ARG + 1, 'manage'),
    'type' => MENU_LOCAL_TASK,
    'file' => 'media.derivatives.forms.inc',
  );
  $items[MEDIA_PATH .'/%media/derivatives/%mapi_derivative/upload'] = array(
    'title' => 'Upload',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mmedia_derivatives_upload_form', MEDIA_ARG + 1, MEDIA_ARG + 3),
    'access callback' => '_mmedia_access',
    'access arguments' => array(MEDIA_ARG + 1, 'manage'),
    'type' => MENU_CALLBACK,
    'file' => 'media.derivatives.forms.inc',
  );
}

// now for the folder related details
$items[FOLDER_PATH] = array(
  'title' => 'Folders',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_view_form', null, null),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(NULL, 'access'),
  'type' => MENU_NORMAL_ITEM,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/%folder'] = array(
  'title callback' => '_mmedia_folder_title',
  'title arguments' => array(FOLDER_ARG + 1),
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_view_form', FOLDER_ARG + 1),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(FOLDER_ARG + 1, 'access'),
  'type' => MENU_NORMAL_ITEM,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/add'] = array(
  'title' => 'Create Folder',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_edit_form', null, null),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(NULL, 'manage'),
  'type' => MENU_NORMAL_ITEM,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/%folder/view'] = array(
  'title' => 'View',
  'type' => MENU_DEFAULT_LOCAL_TASK,
);

$items[FOLDER_PATH .'/%folder/add'] = array(
  'title' => 'Add',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_edit_form', null, FOLDER_ARG + 1),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(FOLDER_ARG + 1, 'manage'),
  'type' => MENU_LOCAL_TASK,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/%folder/edit'] = array(
  'title' => 'Edit',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_edit_form', FOLDER_ARG + 1, null),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(FOLDER_ARG + 1, 'manage'),
  'type' => MENU_LOCAL_TASK,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/%folder/delete'] = array(
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_delete_form', FOLDER_ARG + 1),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(FOLDER_ARG + 1, 'manage'),
  'type' => MENU_CALLBACK,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/%folder/move'] = array(
  'title' => 'Move',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_folder_move_form', FOLDER_ARG + 1),
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(FOLDER_ARG + 1, 'manage'),
  'type' => MENU_LOCAL_TASK,
  'file' => 'folder.forms.inc',
);

$items[FOLDER_PATH .'/autocomplete'] = array(
  'title' => 'autocomplete',
  'page callback' => 'mmedia_folder_autocomplete',
  'access callback' => '_mmedia_folder_access',
  'access arguments' => array(NULL, 'access'),
  'type' => MENU_CALLBACK,
  'file' => 'folder.pages.inc',
);
