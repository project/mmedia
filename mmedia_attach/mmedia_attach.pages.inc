<?php

/**
 * View the media attached to the page.
 */
function mmedia_attach_view_page($node) {
  // display the media attached to the node
  // get the list of media attached.
  // similar playlist nodes grouped together ()
  // then everything else simply just displayed with a given profile.
  // this should display the media
  $list = array();
  $output = '';

  foreach ($node->media as $aid => $mid) {
    $object = media_attach_load($aid);
    $media = media_load($mid);

    if ($object->visible) {
      $profile = variable_get('mmedia_attach_view_profile', null);
      $list[$aid]->media = $media;
      $list[$aid]->display = mapi_display(media_filename($media), array('profile' => $profile));
      $list[$aid]->caption = check_markup($object->caption, FILTER_FORMAT_DEFAULT, false);
    }
  }

  return theme('mmedia_attach_view_page', $list);
}

/**
 * Detach the media attached to a node.
 */
function mmedia_attach_media_detach($node, $attach) {
  // detach media from the node given
  if ($attach->nid == $node->nid) {
    media_detach($node->nid, $attach->aid);
  }
  drupal_goto('node/'. $node->nid .'/media/list');
  exit();
}
