<?php

/**
 * @file mmedia-attach-attach-form.tpl.php
 * Default theme implementation to manage node attached media.
 *
 */

?>
<?php print $search; ?>
<?php print $filter; ?>
<?php print $pager; ?>
<table id="node-media" class="sticky-enabled">
  <thead>
    <tr>
      <th>&nbsp;</th>
      <th><?php print t('Preview'); ?></th>
      <th><?php print t('Media'); ?></th>
      <th><?php print t('Type'); ?></th>
      <th><?php print t('Extension'); ?></th>
      <th><?php print t('Created'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php $row = 0; ?>
    <?php foreach ($media as $mid => $data): ?>
      <tr class="<?php print $row % 2 == 0 ? 'odd' : 'even'; ?>">
        <td><?php print $data->attach ?></td>
        <td><?php print $data->preview; ?></td>
        <td><?php print $data->media; ?></td>
        <td><?php print $data->type; ?></td>
        <td><?php print $data->ext; ?></td>
        <td><?php print $data->created; ?></td>
      </tr>
      <?php $row++; ?>
    <?php endforeach; ?>
  </tbody>
</table>
<?php print $pager; ?>
<?php print $form_submit; ?>
