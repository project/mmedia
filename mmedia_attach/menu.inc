<?php

$items['node/%node/media'] = array(
  'title' => 'Media',
  'page callback' => 'mmedia_attach_view_page',
  'page arguments' => array(1),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'view'),
  'file' => 'mmedia_attach.pages.inc',
  'type' => MENU_LOCAL_TASK,
);

$items['node/%node/media/view'] = array(
  'title' => 'View',
  'type' => MENU_DEFAULT_LOCAL_TASK,
);

$items['node/%node/media/list'] = array(
  'title' => 'List',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_attach_edit_form', 1),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'edit'),
  'file' => 'mmedia_attach.forms.inc',
  'type' => MENU_LOCAL_TASK,
);

// attach a media to a node
$items['node/%node/media/attach'] = array(
  'title' => 'Attach',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_attach_attach_form', 1),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'edit'),
  'file' => 'mmedia_attach.forms.inc',
  'type' => MENU_LOCAL_TASK,
);

// upload a media to a node
$items['node/%node/media/upload'] = array(
  'title' => 'Upload',
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_attach_upload_form', 1),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'upload'),
  'file' => 'mmedia_attach.forms.inc',
  'type' => MENU_LOCAL_TASK,
);

// allows changing of the caption
$items['node/%node/media/%media_attach'] = array(
  'page callback' => 'drupal_get_form',
  'page arguments' => array('mmedia_attach_item_form', 1, 3),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'edit'),
  'file' => 'mmedia_attach.forms.inc',
  'type' => MENU_CALLBACK,
);

// detach a media from a node
$items['node/%node/media/%media_attach/detach'] = array(
  'page callback' => 'mmedia_attach_media_detach',
  'page arguments' => array(1, 3),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'edit'),
  'file' => 'mmedia_attach.pages.inc',
  'type' => MENU_CALLBACK,
);

// allow the popup to insert media
$items['node/%node/media/popup'] = array(
  'page callback' => 'mmedia_attach_configure_page',
  'page arguments' => array(1),
  'access callback' => '_mmedia_attach_access',
  'access arguments' => array(1, 'edit'),
  'file' => 'mmedia_attach.forms.inc',
  'type' => MENU_CALLBACK,
);

