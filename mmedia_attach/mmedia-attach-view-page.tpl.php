<?php

/**
 * @file mmedia-attach-form.tpl.php
 * Default theme implementation to manage node attached media.
 *
 */
?>
<?php foreach ($items as $item): ?>
  <div><?php print $item->display; ?></div>
  <div><?php print $item->caption; ?></div>
<?php endforeach ?>