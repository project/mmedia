<?php

/**
 * @file mmedia-attach-form.tpl.php
 * Default theme implementation to manage node attached media.
 *
 */
?>
<?php
  drupal_add_tabledrag('node-media', 'order', 'sibling', 'attached-weight');
?>
<table id="node-media" class="sticky-enabled">
  <thead>
    <tr>
      <th><?php print t('Caption'); ?></th>
      <th><?php print t('Download'); ?></th>
      <th><?php print t('Visible'); ?></th>
      <th><?php print t('Playlist'); ?></th>
      <th><?php print t('Weight'); ?></th>
      <th colspan="2"><?php print t('Operations'); ?></th>
    </tr>
  </thead>
  <tbody>
    <?php $row = 0; ?>
    <?php foreach ($media as $aid => $data): ?>
      <tr class="draggable <?php print $row % 2 == 0 ? 'odd' : 'even'; ?>">
        <td class="attached"><?php print $data->caption ?></td>
        <td><?php print $data->download; ?></td>
        <td><?php print $data->visible; ?></td>
        <td><?php print $data->playlist; ?></td>
        <td><?php print $data->weight_select; ?></td>
        <td><?php print $data->operations; ?></td>
      </tr>
      <?php $row++; ?>
    <?php endforeach; ?>
  </tbody>
</table>

<?php print $form_submit; ?>
