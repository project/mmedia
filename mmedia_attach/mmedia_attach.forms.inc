<?php

/**
 *  Basic edit of caption for the attached item
 */
function mmedia_attach_item_form($form_state, $node, $attached) {
  if ($node->nid != $attached->nid) {
    drupal_not_found();
    exit();
  }

  // set as the title of the page.
  drupal_set_title(check_plain($node->title));

  $form = array(
    'nid' => array('#type' => 'value', '#value' => $node->nid),
    'aid' => array('#type' => 'value', '#value' => $attached->aid),
    'caption' => array('#type' => 'textarea', '#title' => t('Caption'), '#default_value' => $attached->caption, '#required' => true),
    'display' => array('#type' => 'markup' , '#value' => '<div>'. mapi_display(media_filename(media_load($attached->mid))) .'</div>'),
    'submit' => array('#type' => 'submit', '#value' => t('Submit')),
  );

  return $form;
}

function mmedia_attach_item_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Submit')) {
    db_query("UPDATE {media_attachments} SET caption = '%s' WHERE aid = %d", $values['caption'], $values['aid']);
  }
  $form_state['redirect'] = 'node/'. $values['nid'] .'/media/list';
}

/**
 *  Basic attach form gallery.
 */
function mmedia_attach_edit_form($form_state, $node) {
  $form = array();
  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);

  $form['media'] = array(
    '#tree' => true,
  );

  foreach (array_keys($node->media) as $aid) {
    $attach = media_attach_load($aid);
    // setup download ability dependent on the derivative list
    $download = array(0 => t('none'), 'o' => t('original'));
    if (module_exists('mapi_derivatives')) {
      $media = media_load($attach->mid);
      $download = array_merge($download, mapi_derivative_list_by_extension($media->ext));
    }

    $form['media'][$aid] = array(
      'caption' => array('#value' => l($attach->caption, 'node/'. $node->nid .'/media/'. $aid)),
      'download' => array('#type' => 'select', '#options' => $download, '#default_value' => (is_null($attach->download) ? 'o' : $attach->download)),
      'visible' => array('#type' => 'checkbox', '#default_value' => $attach->visible),
      'playlist' => array('#type' => 'checkbox', '#default_value' => $attach->playlist),
      'weight' => array('#type' => 'weight', '#default_value' => $attach->weight, '#delta' => 100),
      'detach' => array('#value' => l(t('Detach'), 'node/'. $node->nid .'/media/'. $aid .'/detach')),
    );
  }

  if (count($node->media)) {
    $form['save_changes'] = array('#type' => 'submit', '#value' => t('Save Changes'));
  }

  return $form;
}

/**
 * Edit submission form
 */
function mmedia_attach_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $nid = $values['nid'];
  foreach ($values['media'] as $aid => $data) {
    // save download as null
    if ($data['download'] == 'o') {
      // updates the appropriate details
      db_query("UPDATE {media_attachments} SET download = null, playlist = %d, visible = %d, weight = %d WHERE aid = %d AND nid = %d",
        $data['playlist'], $data['visible'], $data['weight'], $aid, $nid);
    }
    else {
      // updates the appropriate details
      db_query("UPDATE {media_attachments} SET download = %d, playlist = %d, visible = %d, weight = %d WHERE aid = %d AND nid = %d",
        $data['download'], $data['playlist'], $data['visible'], $data['weight'], $aid, $nid);
    }
  }
}

/**
 *  Upload form
 */
function mmedia_attach_upload_form($form_state, $node) {
  // provide the upload for a media file.
  // with all the assorted extras

  // find the weight it will end up being for the node
  $weight = db_result(db_query("SELECT MAX(weight) FROM {media_attachments} WHERE nid = %d", $node->nid));
  // make sure it's the heaviest.
  $weight += 1;

  $edit = new stdClass;
  $edit->type = null;

  $form = _mmedia_form($edit);

  // extra details required for media attaching
  $form['playlist'] = array(
    '#type' => 'checkbox',
    '#title' => t('Playlist'),
    '#default_value' => false,
    '#description' => t('Adds media to attached playlist.'),
  );

  $download = array(0 => t('none'), 'o' => t('original'));
  if (module_exists('mapi_derivatives')) {
    $media = media_load($attach->mid);
    $download = array_merge($download, mapi_derivative_list_by_extension($media->ext));
  }

  $form['download'] = array(
    '#type' => 'select',
    '#title' => t('Download'),
    '#options' => $download,
    '#default_value' => 0,
    '#description' => t('If installed, allows a derivative to be downloadable, otherwise provides original and none.'),
  );

  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);
  $form['weight'] = array('#type' => 'value' , '#value' => $weight);
  $form['upload'] = array('#type' => 'submit', '#value' => t('Upload'));

  return $form;
}

/**
 *  Validation of the media form
 */
function mmedia_attach_upload_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  _mmedia_form_process($values);
}

/**
 *  Submission for the Edit Form
 */
function mmedia_attach_upload_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $media = _mmedia_form_post($values);
  if ($media->mid) {
    $options = array(
      'playlist' => $values['playlist'],
      'download' => $values['download'] == 'o' ? null : $values['download'],
    );

    // now add the media to the end of the node
    $attached = media_attach($values['nid'], $media->mid, $media->title, $options);
    $form_state['mid'] = $media->mid;
    $form_state['redirect'] = 'node/'. $values['nid'] .'/media';
  }
}

/**
 * Attach form
 */
function mmedia_attach_attach_form($form_state, $node) {
  // check gets first, then combined leftovers for next page
  $filter = isset($_GET['filter']) ? (object)$_GET['filter'] : null;
  $search = isset($_GET['search']) ? (object)$_GET['search'] : null;
  if (!$filter) {
    $filter = isset($form_state['storage']['filter']) ? (object)$form_state['storage']['filter'] : null;
  }
  if (!$search) {
    $search = isset($form_state['storage']['search']) ? (object)$form_state['storage']['search'] : null;
  }

  $form = array();
  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);

  // provide the standard search routines for attaching media.
  $form['search'] = array(
    '#type' => 'fieldset',
    '#title' => t('Search'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#tree' => true,
  );

  $form['search']['input'] = array(
    '#type' => 'textfield',
    '#title' => t('Search for'),
    '#description' => t('This field can involve numbers, or strings, including wildcards (* - search for multiple characters, ? - search for a single character).'),
    '#default_value' => $search->input,
  );

  $search_options = array(
    'mid' => t('by Media ID'),
    'fid' => t('by Folder ID'),
    'title' => t('by Title'),
    'name' => t('by Filename'),
    'path' => t('by Path'),
  );

  $form['search']['style'] = array(
    '#type' => 'select',
    '#options' => $search_options,
    '#default_value' => $search->style,
  );

  $form['search']['search_button'] = array('#type' => 'submit', '#value' => t('Search'));

  // provide the filtering options for the media
  $form['filter'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter By'),
    '#collapsible' => true,
    '#collapsed' => true,
    '#tree' => true
  );

  $exts = mapi_extension_list();
  if (count($exts)) {
    $form['filter']['ext'] = array(
      '#type' => 'select',
      '#multiple' => true,
      '#title' => t('Extension'),
      '#options' => drupal_map_assoc($exts),
      '#size' => 5,
      '#default_value' => $filter->ext,
    );
  }

  $types = mapi_type_list();
  if (count($types)) {
    $form['filter']['type'] = array(
      '#type' => 'select',
      '#multiple' => true,
      '#title' => t('Type'),
      '#options' => drupal_map_assoc($types),
      '#size' => 5,
      '#default_value' => $filter->type,
    );
  }

  $form['filter']['filter_button'] = array('#type' => 'submit', '#value' => t('Filter'));

  // we already have something we can search by
  $media = array();
  $values = array();
  if ($filter || $search) {
    // construct the filtered, searched SQL
    $wheres = array();

    // figure out search options
    if ($search && $search->input && in_array($search->style, array('mid', 'fid', 'title', 'name', 'path'))) {
      $wildcards = false;
      // simple number
      if (is_numeric($search->input)) {
        $input = $search->input;
      }
      // check wildcards, etc
      else {
        $input = $search->input;
        $wildcards = (strpos($input, '*') !== false) || (strpos($input, '?') !== false);
        $input = str_replace(array('%', '_'), array('\\%', '\\_'), $input);
        $input = str_replace(array('*', '?'), array('%', '_'), $input);
      }

      // provide for generic ways to enter the search style
      $wheres[] = is_numeric($input) ? "m.{$search->style} = %d" : ($wildcards ? "m.{$search->style} LIKE '%s'" : "m.{$search->style} = '%s'");
      $values[] = $input;
    }

    // figure out filter options (should keep it quicker)
    if ($filter) {
      // extensions
      $exts = array_keys(array_filter($filter->ext));
      if (count($exts)) {
        $wheres[] = count($exts) == 1 ? "m.ext = '%s'" : 'm.ext IN ('. db_placeholders($exts, 'varchar') .')';
        $values = array_merge($values, $exts);
      }

      $types = array_keys(array_filter($filter->type));
      if (count($types)) {
        $wheres[] = count($types) == 1 ? "m.type = '%s'" : 'm.type IN ('. db_placeholders($exts, 'varchar') .')';
        $values = array_merge($values, $types);
      }
    }

    $sql = 'SELECT m.mid FROM {media} m ';
    if (count($wheres)) {
      $sql .= 'WHERE ('. implode(') AND (', $wheres) .') ';
    }
    $sql .= 'ORDER BY m.created DESC';
  }
  // otherwise display the latest number of items
  else {
    $sql = 'SELECT m.mid FROM {media} m WHERE m.public <> 0 ORDER BY m.created DESC';
  }

  // now get the list of the appropriate media items
  $result = pager_query(db_rewrite_sql($sql), variable_get('mmedia_default_main_page', 10), 0, null, $values);
  while ($row = db_fetch_object($result)) {
    $media[] = $row->mid;
  }
  $form['pager'] = array('#value' => theme('pager', NULL, variable_get('mmedia_default_main_page', 10)));

  $form['media'] = array(
    '#tree' => true,
  );

  foreach ($media as $mid) {
    $media = media_load($mid);
    $form['media'][$mid] = array(
      'attach' => array('#type' => 'checkbox'),
      'preview' => array('#value' => 'preview'),
      'media' => array('#value' => l($media->title, MEDIA_PATH .'/'. $media->mid)),
      'type' => array('#value' => $media->type),
      'ext' => array('#value' => $media->ext),
      'created' => array('#value' => format_date($media->created, 'short')),
    );
  }

  $form['search_button'] = array('#type' => 'submit', '#value' => t('Search'));
  $form['reset_button'] = array('#type' => 'submit', '#value' => t('Reset'));
  if (count($media)) {
    $form['attach'] = array('#type' => 'submit', '#value' => t('Attach'));
  }

  return $form;
}

/**
 * Submit Form
 */
function mmedia_attach_attach_form_submit($form, &$form_state) {
  $values = &$form_state['values'];
  // attaches the selected media items to the node
  if ($values['op'] == t('Attach')) {
    $nid = $values['nid'];
    $found = false;
    foreach ($values['media'] as $mid => $item) {
      if ($item['attach']) {
        $media = media_load($mid);
        media_attach($nid, $mid, $media->title);
        $values[$mid]['attach'] = 0;
        $found = true;
      }
    }
    if ($found) {
      drupal_set_message(t('Selected media attached to node.'));
    }
  }
  elseif ($values['op'] == t('Reset')) {
    unset($form_state['storage']['filter']);
    unset($form_state['storage']['search']);
  }
  // otherwise, make sure the form becomes filtered and searched by
  else {
    $form_state['storage']['filter'] = $values['filter'];
    $form_state['storage']['search'] = $values['search'];
  }
}

/**
 * Configure a tag
 */
function mmedia_attach_configure_page($node, $extra = null) {
  $content = drupal_get_form('mmedia_attach_configure_form', $node, $extra);
  print theme('mmedia_attach_configure_page', $content);
}

function mmedia_attach_configure_form($form_state, $node, $extra = null) {
  // Since we don't have anything to configure, we need to find something from the list of available
  // items.
  if (isset($form_state['storage']['data'])) {
    $data = (object)$form_state['storage']['data'];
  }

  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);

  switch ($data->step) {
    default:
    case 0:
      $list = array();
      foreach ($node->media as $aid => $mid) {
        $attached = media_attach_load($aid);
        $list[$aid] = $attached->caption;
      }

      if (count($list)) {
        $form['mid'] = array(
          '#type' => 'select',
          '#title' => t('Attached Media'),
          '#options' => $list,
          '#description' => t('Select media to be inserted into the textarea. Use ctrl for multiple selection.'),
          '#required' => true,
          '#multiple' => true,
          '#default_value' => $data->mid,
        );
        $form['style'] = array(
          '#type' => 'select',
          '#title' => t('Style'),
          '#options' => drupal_map_assoc(array_keys(media_attach_tag_list())),
          '#description' => t('Selects what tag style the attached media will be displayed as.'),
          '#default_value' => $data->style,
        );
        $form['submit'] = array('#type' => 'submit', '#value' => t('Next'));
      }
      else {
        $form['mid'] = array('#value' => t('This node has no media attached.'));
      }
      break;
    // step one includes handling the listed media.
    case 1:
      $form['settings'] = mmedia_attach_invoke('configure', $data->mid, $data->style, null, null);
      $list = array_merge(array(0 => t('none')), drupal_map_assoc(mapi_profile_list()));
      if (count($list)) {
        $form['profile'] = array(
          '#type' => 'select',
          '#title' => t('Profile'),
          '#options' => $list,
          '#default_value' => $data->profile,
          '#description' => t('What profile do you wish to use with this tag?'),
        );
      }
      $form['mid'] = array('#type' => 'value', '#value' => $data->mid);
      $display = mmedia_attach_invoke('process', $data->mid, $data->style, array(), $data->profile);
      $display = $display ? $display : t('The tag doesn\'t apply to the selected media.');
      $form['display'] = array('#weight' => -20, '#value' => '<div>'. $display .'</div>');

      $form['back'] = array('#type' => 'submit', '#value' => t('Back'));
      $form['preview'] = array('#type' => 'submit', '#value' => t('Preview'));
      $form['insert'] = array('#type' => 'submit', '#value' => t('Insert'));
      break;
  }

  return $form;
}

/**
 *
 */
function mmedia_attach_configure_form_validate($form, &$form_state) {
}

/**
 * Submission for mmedia_attach_configure_form.
 */
function mmedia_attach_configure_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  switch ($values['op']) {
    case t('Next'):
      // copy the aid and media item for the attached media to process correctly.
      $node = node_load($values['nid']);
      foreach ($values['mid'] as $aid) {
        $form_state['storage']['data']['mid'][$aid] = $node->media[$aid];
      }
      $form_state['storage']['data']['style'] = $values['style'];
      $form_state['storage']['data']['step'] = 1;
      break;
    case t('Back'):
      $form_state['storage']['data']['step'] = 0;
      $form_state['storage']['data']['preview'] = 0;
      break;
    case t('Preview'):
      $form_state['storage']['data']['preview'] = 1;
      $form_state['storage']['data']['settings'] = $values['settings'];
      $form_state['storage']['data']['profile'] = $values['profile'];
      break;
    case t('Insert'):
      // need to return some kind of insert data for this.
      drupal_set_message("Something should show up here.");
      // produce the tagged details.
      break;
  }
}
