<?php

/**
 * Implementation of hook_mapi_admin().
 */
function mmedia_attach_mapi_admin($op) {
  // handle details., both theming and menu options.
  switch ($op) {
    // return the settings for use by the mapi_admin module.
    case 'settings':
      $profiles = mapi_profile_list($profile);
      if (count($profiles)) {
        $items['attach'] = array(
          '#type' => 'fieldset',
          '#title' => t('Attached Media'),
          '#collapsible' => TRUE,
        );

        $items['attach']['mmedia_attach_view_profile'] = array(
          '#type' => 'checkbox',
          '#title' => t('Description'),
          '#default_value' => variable_get('mmedia_attach_view_profile', false),
        );
      }
      return $items;
      break;
  }
}

