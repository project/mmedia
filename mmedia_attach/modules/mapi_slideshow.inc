<?php

/**
 *  Implementation of hook_media_attach().
 */
function mapi_slideshow_media_attach($op, $tag, $settings, $context, $full) {
  switch ($op) {
    case 'list':
      return array('slideshow');

    case 'info':
      return array('style' => 'single');

    case 'process':
      // we don't have a node
      if (empty($context['nid'])) {
        return;
      }
      $list = media_attach_node_list($context['nid']);

      // check what index the image refers to.
      $slideshow = array();

      // check what index the image refers to.
      $profile = media_default_profile($context, $settings['profile']);

      // go through the entire list of attached items, looking for an 'image' media.
      foreach ($list as $aid => $mid) {
        $attached = media_attach_load($aid);
        $media = media_load($mid);

        if ($media->type == 'image') {
          // only display it, if  is indexed, or if it is the first one on the list.
          $slideshow[] = array(
            'filename' => media_filename($media),
            'metadata' => array('title' => $attached->caption),
          );
        }
      }

      if (count($slideshow)) {
        return mapi_slideshow_generate('attached', $items, $settings, $profile);
      }

      return '';
      break;
  }
}
