<?php

/**
 * Implementation of hook_filter_macros().
 */
function mmedia_attach_filter_macros($op, $tag) {
  static $cache;

  $settings = !empty($tag['settings'][0]) ? $tag['settings'][0] : array();

  if (!isset($cache)) {
    $cache = array();
    $cache = media_attach_tag_list();
  }

  switch ($op) {
    case 'list':
      return array_keys($cache);
      break;
    default:
      if (!array_key_exists($tag['tag'], $cache)) {
        return module_invoke_all('media_attach', $op, $tag['tag'], $settings, $tag['context'], $tag['full']);
      }
      return module_invoke($cache[$tag['tag']], 'media_attach', $op, $tag['tag'], $settings, $tag['context'], $tag['full']);
      break;
  }
}
