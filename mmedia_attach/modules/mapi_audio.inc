<?php

/**
 *  Implementation of hook_media_attach().
 */
function mapi_audio_media_attach($op, $tag, $settings, $context, $full) {
  switch ($op) {
    case 'list':
      return array('audio');

    case 'info':
      return array('style' => 'single');

    case 'process':
      // we don't have a node
      if (empty($context['nid'])) {
        return;
      }

      $list = media_attach_node_list($context['nid']);

      // check what index the image refers to.
      $index = isset($settings[0]) && is_numeric($settings[0]) ? $settings[0] : 0;
      $count = 1;
      $profile = media_default_profile($context, $settings['profile']);

      // go through the entire list of attached items, looking for an 'image' media.
      foreach ($list as $aid => $mid) {
        $attached = media_attach_load($aid);
        $media = media_load($mid);

        if ($media->type == 'audio') {
          // only display it, if  is indexed, or if it is the first one on the list.
          if (($index && $count == $index) || !$index) {
            $options = array('settings' => $settings);
            if ($profile) {
              $options['profile'] = $profile;
            }
            if (isset($settings['layout'])) {
              $options['layout'] = $settings['layout'];
              if ($settings['float'] && in_array($settings['float'], array('left', 'right'))) {
                $options['float'] = $settings['float'];
              }
              $options['metadata'] = array('title' => $attached->caption);
            }
            if ($full['content']) {
              $options['metadata']['caption'] = $full['content'];
            }
            return mapi_display(media_filename($media), $options);
          }
          $count += 1;
        }
      }

      return '';
      break;
  }
}
