<?php

/**
 *  The View media form
 */
function mmedia_view_form(&$form_state, $media) {
  // allow various other modules to do things to view
  $profile = variable_get('mmedia_view_profile', NULL);
  $profile = ($profile && in_array($profile, mapi_profile_list()) ? array('profile' => $profile) : array());

  drupal_set_title(t($media->title));
  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  drupal_set_breadcrumb($breadcrumbs);

  // allows for overriding the view form easily
  $form['#media'] = $media;

  // display the result
  $form['display'] = array(
    '#prefix' => '<div class="media-content">',
    '#value' => mapi_display(media_filename($media), $profile),
    '#suffix' => '</div>',
  );

  $display = media_invoke_metadata('view', media_metadata_load($media->mid));
  if ($display) {
    $form['metadata'] = array(
      '#prefix' => '<div class="media-content">',
      '#suffix' => '</div>',
    );

    $form['metadata'] = array_merge($form['metadata'], $display);
  }

  return $form;
}

/**
 *  The Edit/Add media form
 */
function mmedia_edit_form(&$form_state, $media, $type = NULL) {
  // useful for editing the details of a media file.
  $edit = $media;
  if (is_null($edit)) {
    $edit = new stdClass;
    $edit->type = NULL;
    if (in_array($type, mapi_type_list($type))) {
      $edit->type = $type;
    }
  }
  else {
    $media->filename = media_filename($media);
    $media->metadata = media_metadata_load($media->mid);
  }

  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  if ($media) {
    $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  }
  drupal_set_breadcrumb($breadcrumbs);

  // generate the form give the correct
  $form = _mmedia_form($edit);

  // add the buttons
  if (is_null($media)) {
    $form['preview'] = array('#type' => 'button', '#value' => t('Preview'));
  }
  $form['submit'] = array('#type' => 'submit', '#value' => $media->mid ? t('Update') : t('Create'));
  if (!is_null($media)) {
    $form['delete'] = array('#type' => 'button', '#value' => t('Delete'));
  }

  return $form;
}

/**
 *  Validation of the media form
 */
function mmedia_edit_form_validate($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Delete')) {
    drupal_goto(MEDIA_PATH .'/'. $values['mid'] .'/delete');
    exit();
  }
  _mmedia_form_process($values);
}

/**
 *  Submission for the Edit Form
 */
function mmedia_edit_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $media = _mmedia_form_post($values);
  if ($media->mid) {
    $form_state['mid'] = $media->mid;
    $form_state['redirect'] = MEDIA_PATH .'/'. $media->mid;
  }
}

/**
 *  Deletion of media form.
 */
function mmedia_delete_form($form_state, $media) {
  // load the media item
  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  drupal_set_breadcrumb($breadcrumbs);

  // remember for confirmation of the media
  $form['mid'] = array('#type' => 'value', '#value' => $media->mid);

  // provide user interaction
  $notice = t('This action cannot be undone.');

  $form = confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $media->title)),
    isset($_GET['destination']) ? $_GET['destination'] : MEDIA_PATH ."/{$mid}/view",
    $notice,
    t('Delete'), t('Cancel'));

  return $form;
}

/**
 *  Submission for media delete
 */
function mmedia_delete_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['confirm'] == 1) {
    $mid = $values['mid'];
    media_delete($mid);
  }

  $form_state['redirect'] = '<front>';
}

/**
 *  Move media from one folder to another.
 */
function mmedia_move_form(&$form_state, $media) {
  drupal_add_css(drupal_get_path('module', 'mmedia') .'/mmedia.css');

  $breadcrumbs = _mmedia_set_breadcrumb(mmedia_folder_load($media->fid));
  $breadcrumbs[] = l($media->title, MEDIA_PATH .'/'. $media->mid);
  drupal_set_breadcrumb($breadcrumbs);

  $folders = _mmedia_folder_cache();

  // add everything else
  $current = 0;
  $lines = $folders['line'];
  foreach ($lines as $line => $fid) {
    $depth = (count(explode('/', $line)) - FOLDER_PATH_ARG - 1);

    // add the appropriate boundaries for accordian viewing
    $prefix = '';
    while ($depth > $current++) {
      $prefix .= '<div class="folder-level">';
    }
    while ($current > $depth + 1) {
      $prefix .= '</div>';
      $current--;
    }

    // generate the radio buttons for the various items
    $form['radios'][$line] = array(
      '#prefix' => $prefix .'<div class="folder-level-'. $depth .'">',
      '#type' => 'radio',
      '#title' => $folders[$fid]['name'],
      '#return_value' => $fid,
      '#default_value' => ($fid == $media->fid ? $fid : FALSE),
      '#parents' => array('folder'),
      '#suffix' => '</div>'
    );
    $current = $depth;
  }
  $form['radios']['#suffix'] = '';
  while ($current > 0) {
    $form['radios']['#suffix'] .= '</div>';
    $current--;
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Move'));
  $form['mid'] = array('#type' => 'value', '#value' => $media->mid);

  return $form;
}

/**
 *  Submission for the move form
 */
function mmedia_move_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  if ($values['op'] == t('Move')) {
    media_move($values['mid'], $values['folder']);
  }
}

