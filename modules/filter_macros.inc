<?php

/**
 * Implementation of hook_filter_macros().
 */
function mmedia_filter_macros($op, $tag) {
  switch ($op) {
    // list of supported tags
    case 'list':
      return array('media', 'media-mid', 'media-metadata', 'media-url', 'media-ext', 'media-filesize');

    // information about tag
    case 'info':
      return array('style' => 'single');

    // process media embeds
    case 'process':
      $context = $tag['context'];
      $settings = !empty($tag['settings'][0]) ? $tag['settings'][0] : array();
      if (!empty($context['context']) && $context['context'] == 'media' && !empty($context['mid'])) {
        $mid = $context['mid'];
      }
      elseif (isset($settings['mid']) && is_numeric($settings['mid'])) {
        $mid = $settings['mid'];
      }
      elseif (isset($settings[0]) && is_numeric($settings[0])) {
        $mid = $settings[0];
      }
      else {
        $mid = 0;
      }

      if ($mid && ($media = media_load($mid))) {
        switch ($tag['tag']) {
          case 'media':
            $profile = media_default_profile($context, $settings['profile']);
            $filename = media_filename($media);
            if (module_exists('mapi_derivatives') && !empty($context['did']) && array_key_exists($context['did'], mapi_derivative_list_by_extension($media->ext))) {
              $filename = mapi_derive($filename, mapi_derivative_name($context['did']));
              $profile = null;
            }
            return mapi_display($filename, array('profile' => $profile, 'settings' => $settings));
            break;
          case 'media-mid':
            return $media->mid;
            break;
          case 'media-ext':
            return $media->ext;
            break;
          case 'media-filesize':
            $sizes = array('kb', 'mb', 'gb');
            $ext = '';
            $size = filesize($filename);
            while ($size > 1000 && count($sizes)) {
              $size /= 1000;
              $ext = array_shift($sizes);
            }
            return round($size, 1) . $ext;
            break;
          case 'media-metadata':
            break;
          case 'media-url':
            $filename = media_filename($media);
            if (module_exists('mapi_derivatives') && !empty($context['did']) && array_key_exists($context['did'], mapi_derivative_list_by_extension($media->ext))) {
              $filename = mapi_derive($filename, mapi_derivative_name($context['did']));
            }
            return $filename ? mapi_url($filename) : '';
            break;
        }
      }

      // display nothing
      return '';
  }
}
