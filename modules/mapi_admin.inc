<?php

/**
 * Implementation of hook_mapi_admin().
 */
function mmedia_mapi_admin($op) {
  // handle details., both theming and menu options.
  switch ($op) {
    // handle the menu item
    case 'menu':
      return array(
        'admin/mapi/media' => array(
          'title' => t('Media'),
          'description' => t("View, edit, and delete your site's media."),
          'page callback' => 'drupal_get_form',
          'page arguments' => array('mmedia_admin_content'),
          'access callback' => 'user_access',
          'access arguments' => array('administer media'),
          'type' => MENU_NORMAL_ITEM,
          'file' => 'mapi_admin.inc',
          'file path' => drupal_get_path('module', 'mmedia') .'/modules',
        )
      );
      break;

    // return the themes that are used by this module
    case 'theme':
      return array(
        'mmedia_filter_form' => array(
          'arguments' => array('form' => NULL),
          'file' => 'mapi_admin.inc',
          'path' => drupal_get_path('module', 'mmedia') .'/modules',
        ),
        'mmedia_filters' => array(
          'arguments' => array('form' => NULL),
          'file' => 'mapi_admin.inc',
          'path' => drupal_get_path('module', 'mmedia') .'/modules',
        ),
        'mmedia_admin_media' => array(
          'arguments' => array('form' => NULL),
          'file' => 'mapi_admin.inc',
          'path' => drupal_get_path('module', 'mmedia') .'/modules',
        ),
      );
      break;

    // return the settings for use by the mapi_admin module.
    case 'settings':
      $items['metadata'] = array(
        '#type' => 'fieldset',
        '#title' => t('Metadata'),
        '#collapsible' => TRUE,
      );

      $items['metadata']['mmedia_metadata_description'] = array(
        '#type' => 'checkbox',
        '#title' => t('Description'),
        '#default_value' => variable_get('mmedia_metadata_description', false),
      );

      $items['metadata']['mmedia_metadata_license'] = array(
        '#type' => 'checkbox',
        '#title' => t('License'),
        '#default_value' => variable_get('mmedia_metadata_license', false),
      );

      if (module_exists('date')) {
        $items['metadata']['mmedia_metadata_changed'] = array(
          '#type' => 'checkbox',
          '#title' => t('Changed'),
          '#default_value' => variable_get('mmedia_metadata_changed', false),
        );

        $items['metadata']['mmedia_metadata_expire'] = array(
          '#type' => 'checkbox',
          '#title' => t('Expire'),
          '#default_value' => variable_get('mmedia_metadata_expire', false),
        );
      }

      $items['urls'] = array(
        '#type' => 'fieldset',
        '#title' => t('URLs'),
        '#collapsible' => TRUE,
      );

      $items['urls']['mmedia_default_main_page'] = array(
        '#type' => 'textfield',
        '#title' => t('Media per Page'),
        '#default_value' => variable_get('mmedia_default_main_page', 10),
        '#size' => 5,
        '#maxlength' => 5,
        '#description' => t('The number of media to display per page.')
      );

      $items['urls']['mmedia_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Media URL'),
        '#default_value' => variable_get('mmedia_url', 'media'),
        '#description' => t('This defines the base URL where all the media related handling is located.')
      );

      $items['urls']['mmedia_folder_page'] = array(
        '#type' => 'textfield',
        '#title' => t('Folder Page'),
        '#default_value' => variable_get('mmedia_folder_page', 10),
        '#size' => 5,
        '#maxlength' => 5,
        '#description' => t('The number of media to be displayed for a folder page.')
      );

      $items['urls']['mmedia_folder_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Folder URL'),
        '#default_value' => variable_get('mmedia_folder_url', 'folder'),
        '#description' => t('The base URL for where the folder handling is located.')
      );

      $items['folders'] = array(
        '#type' => 'fieldset',
        '#title' => t('Folders'),
        '#collapsible' => TRUE,
      );

      $items['folders']['mmedia_dir_public'] = array(
        '#type' => 'textfield',
        '#title' => t('Public Media Directory'),
        '#default_value' => variable_get('mmedia_dir_public', 'media'),
        '#description' => t('Location within files directory of where the media will be publicly stored.')
      );

      $items['folders']['mmedia_dir_temp'] = array(
        '#type' => 'textfield',
        '#title' => t('Temporary Media Directory'),
        '#default_value' => variable_get('mmedia_dir_temp', 'temp'),
        '#description' => t('Location of the temporary viewing area for media within the files directory.')
      );

      $items['folders']['mmedia_private'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow Private Storage'),
        '#default_value' => variable_get('mmedia_private', false),
        '#description' => t('Allows media to be stored privately, thereby ensuring the original is not accessed directly through the webserver.'),
      );

      $items['folders']['mmedia_dir_private'] = array(
        '#type' => 'textfield',
        '#title' => t('Private Media Directory'),
        '#default_value' => variable_get('mmedia_dir_private', '../media'),
        '#description' => t('Location of the private media directory. This can be either the full path, or a relative path (which stores it relative to the location of index.php).')
      );

      $list = mapi_profile_list();
      if (count($list)) {
        $items['profile'] = array(
          '#type' => 'fieldset',
          '#title' => t('Profiles'),
          '#collapsible' => TRUE,
        );

        $items['profile']['mmedia_teaser_profile'] = array(
          '#type' => 'select',
          '#title' => t('Teaser profile'),
          '#default_value' => variable_get('mmedia_teaser_profile', null),
          '#options' => drupal_map_assoc($list),
          '#description' => t('The default profile to be used for media within a teaser view.')
        );

        $items['profile']['mmedia_node_profile'] = array(
          '#type' => 'select',
          '#title' => t('Node profile'),
          '#default_value' => variable_get('mmedia_node_profile', null),
          '#options' => drupal_map_assoc($list),
          '#description' => t('The default profile to be used for media within a node view.')
        );

        $items['profile']['mmedia_preview_profile'] = array(
          '#type' => 'select',
          '#title' => t('Preview profile'),
          '#default_value' => variable_get('mmedia_preview_profile', null),
          '#options' => drupal_map_assoc($list),
          '#description' => t('The default profile to be used for previewing media.')
        );

        $items['profile']['mmedia_view_profile'] = array(
          '#type' => 'select',
          '#title' => t('View profile'),
          '#default_value' => variable_get('mmedia_view_profile', null),
          '#options' => drupal_map_assoc($list),
          '#description' => t('The default profile to be used when viewing media specifically.')
        );
      }

      return $items;
      break;
  }
}

/**
 * Implementation of hook_media_operations().
 */
function mmedia_media_operations() {
  $operations = array(
    'delete' => array(
      'label' => t('Delete'),
      'callback' => NULL,
    ),
  );
  return $operations;
}

/**
 * List media administration filters that can be applied.
 */
function mmedia_filters() {
  // filter by extension
  $filters['ext'] = array(
    'title' => t('extension'),
    'options' => drupal_map_assoc(mapi_extension_list()),
  );

  // filter by type
  $filters['type'] = array(
    'title' => t('type'),
    'options' => drupal_map_assoc(mapi_type_list()),
  );

  return $filters;
}

/**
 * Build query for media administration filters based on session.
 */
function mmedia_build_filter_query() {
  $filters = mmedia_filters();

  // Build query
  $where = $args = array();
  $join = '';
  foreach ($_SESSION['mmedia_overview_filter'] as $index => $filter) {
    list($key, $value) = $filter;
    switch ($key) {
      case 'ext':
      case 'type':
        // Note: no exploitable hole as $key/$value have already been checked when submitted
        list($key, $value) = explode('-', $value, 2);
        $where[] = "m.{$key} = '%s'";
        break;
    }
    $args[] = $value;
  }
  $where = count($where) ? 'WHERE '. implode(' AND ', $where) : '';

  return array('where' => $where, 'join' => $join, 'args' => $args);
}

/**
 * Return form for media administration filters.
 */
function mmedia_filter_form() {
  $session = &$_SESSION['mmedia_overview_filter'];
  $session = is_array($session) ? $session : array();
  $filters = mmedia_filters();

  $i = 0;
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Show only items where'),
    '#theme' => 'mmedia_filters',
  );
  $form['#submit'][] = 'mmedia_filter_form_submit';
  foreach ($session as $filter) {
    list($type, $value) = $filter;
    if ($type == 'category') {
      // Load term name from DB rather than search and parse options array.
      $value = module_invoke('taxonomy', 'get_term', $value);
      $value = $value->name;
    }
    else if ($type == 'language') {
      $value = empty($value) ? t('Language neutral') : module_invoke('locale', 'language_name', $value);
    }
    else {
      $value = $filters[$type]['options'][$value];
    }
    if ($i++) {
      $form['filters']['current'][] = array('#value' => t('<em>and</em> where <strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
    }
    else {
      $form['filters']['current'][] = array('#value' => t('<strong>%a</strong> is <strong>%b</strong>', array('%a' => $filters[$type]['title'], '%b' => $value)));
    }
    if (in_array($type, array('type', 'language'))) {
      // Remove the option if it is already being filtered on.
      unset($filters[$type]);
    }
  }

  foreach ($filters as $key => $filter) {
    $names[$key] = $filter['title'];
    $form['filters']['status'][$key] = array('#type' => 'select', '#options' => $filter['options']);
  }

  $form['filters']['filter'] = array('#type' => 'radios', '#options' => $names, '#default_value' => 'status');
  $form['filters']['buttons']['submit'] = array('#type' => 'submit', '#value' => (count($session) ? t('Refine') : t('Filter')));
  if (count($session)) {
    $form['filters']['buttons']['undo'] = array('#type' => 'submit', '#value' => t('Undo'));
    $form['filters']['buttons']['reset'] = array('#type' => 'submit', '#value' => t('Reset'));
  }

  drupal_add_js('misc/form.js', 'core');

  return $form;
}

/**
 * Theme media administration filter form.
 *
 * @ingroup themeable
 */
function theme_mmedia_filter_form($form) {
  $output = '';
  $output .= '<div id="media-admin-filter">';
  $output .= drupal_render($form['filters']);
  $output .= '</div>';
  $output .= drupal_render($form);
  return $output;
}

/**
 * Theme media administration filter selector.
 *
 * @ingroup themeable
 */
function theme_mmedia_filters($form) {
  $output = '';
  $output .= '<ul class="clear-block">';
  if (!empty($form['current'])) {
    foreach (element_children($form['current']) as $key) {
      $output .= '<li>'. drupal_render($form['current'][$key]) .'</li>';
    }
  }

  $output .= '<li><dl class="multiselect">'. (!empty($form['current']) ? '<dt><em>'. t('and') .'</em> '. t('where') .'</dt>' : '') .'<dd class="a">';
  foreach (element_children($form['filter']) as $key) {
    $output .= drupal_render($form['filter'][$key]);
  }
  $output .= '</dd>';

  $output .= '<dt>'. t('is') .'</dt><dd class="b">';

  foreach (element_children($form['status']) as $key) {
    $output .= drupal_render($form['status'][$key]);
  }
  $output .= '</dd>';

  $output .= '</dl>';
  $output .= '<div class="container-inline" id="media-admin-buttons">'. drupal_render($form['buttons']) .'</div>';
  $output .= '</li></ul>';

  return $output;
}

/**
 * Process result from media administration filter form.
 */
function mmedia_filter_form_submit($form, &$form_state) {
  $filters = mmedia_filters();
  switch ($form_state['values']['op']) {
    case t('Filter'):
    case t('Refine'):
      if (isset($form_state['values']['filter'])) {
        $filter = $form_state['values']['filter'];

        // Flatten the options array to accommodate hierarchical/nested options.
        $flat_options = form_options_flatten($filters[$filter]['options']);

        if (isset($flat_options[$form_state['values'][$filter]])) {
          $_SESSION['mmedia_overview_filter'][] = array($filter, $form_state['values'][$filter]);
        }
      }
      break;
    case t('Undo'):
      array_pop($_SESSION['mmedia_overview_filter']);
      break;
    case t('Reset'):
      $_SESSION['mmedia_overview_filter'] = array();
      break;
  }
}

/**
 * Make mass update of medias, changing all media in the $media array
 * to update them with the field values in $updates.
 *
 * IMPORTANT NOTE: This function is intended to work when called
 * from a form submit handler. Calling it outside of the form submission
 * process may not work correctly.
 *
 * @param array $media
 *   Array of media mids to update.
 * @param array $updates
 *   Array of key/value pairs with media field names and the
 *   value to update that field to.
 *  TODO: possibly change this to a metadata modification, etc.
 */
function mmedia_mass_update($media, $updates) {
  // We use batch processing to prevent timeout when updating a large number
  // of nodes.
  if (count($media) > 10) {
    $batch = array(
      'operations' => array(
        array('_mmedia_mass_update_batch_process', array($media, $updates))
      ),
      'finished' => '_mmedia_mass_update_batch_finished',
      'title' => t('Processing'),
      // We use a single multi-pass operation, so the default
      // 'Remaining x of y operations' message will be confusing here.
      'progress_message' => '',
      'error_message' => t('The update has encountered an error.'),
      // The operations do not live in the .module file, so we need to
      // tell the batch engine which file to load before calling them.
      'file' => drupal_get_path('module', 'mmedia') .'/modules/mapi_admin.inc',
    );
    batch_set($batch);
  }
  else {
    foreach ($media as $mid) {
      _mmedia_mass_update_helper($mid, $updates);
    }
    drupal_set_message(t('The update has been performed.'));
  }
}

/**
 * Node Mass Update - helper function.
 */
function _mmedia_mass_update_helper($mid, $updates) {
  // not sure what we can do here, since there isn't actually a media_save

  // TODO: Fix this, to allow changes to the media itself.
  // TODO: Because it's not a simple straight forward process.
  // TODO: It could also include metadata changes.
  drupal_set_message('Attempting to do update to media...('. $mid .')');
  /*
  $media = mmedia_load($mid, TRUE);
  foreach ($updates as $name => $value) {
    $media->$name = $value;
  }
  media_item_save($media);
  */
  $media = media_load($mid, true);

  return $media;
}

/**
 * Node Mass Update Batch operation
 */
function _mmedia_mass_update_batch_process($nodes, $updates, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($nodes);
    $context['sandbox']['nodes'] = $nodes;
  }

  // Process nodes by groups of 5.
  $count = min(5, count($context['sandbox']['media']));
  for ($i = 1; $i <= $count; $i++) {
    // For each nid, load the node, reset the values, and save it.
    $mid = array_shift($context['sandbox']['media']);
    $media = _mmedia_mass_update_helper($mid, $updates);

    // Store result for post-processing in the finished callback.
    $context['results'][] = l($media->title, MEDIA_PATH .'/'. $media->mid);

    // Update our progress information.
    $context['sandbox']['progress']++;
  }

  // Inform the batch engine that we are not finished,
  // and provide an estimation of the completion level we reached.
  if ($context['sandbox']['progress'] != $context['sandbox']['max']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
  }
}

/**
 * Node Mass Update Batch 'finished' callback.
 */
function _mmedia_mass_update_batch_finished($success, $results, $operations) {
  if ($success) {
    drupal_set_message(t('The update has been performed.'));
  }
  else {
    drupal_set_message(t('An error occurred and processing did not complete.'), 'error');
    $message = format_plural(count($results), '1 item successfully processed:', '@count items successfully processed:');
    $message .= theme('item_list', $results);
    drupal_set_message($message);
  }
}

/**
 * Menu callback: content administration.
 */
function mmedia_admin_content($form_state) {
  if (isset($form_state['values']['operation']) && $form_state['values']['operation'] == 'delete') {
    return mmedia_multiple_delete_confirm($form_state, array_filter($form_state['values']['media']));
  }
  $form = mmedia_filter_form();

  $form['#theme'] = 'mmedia_filter_form';
  $form['admin']  = mmedia_admin_media();

  return $form;
}

/**
 * Form builder: Builds the node administration overview.
 */
function mmedia_admin_media() {

  $filter = mmedia_build_filter_query();

  $result = pager_query(db_rewrite_sql('SELECT m.*, u.name AS username FROM {media} m '. $filter['join'] .' INNER JOIN {users} u ON m.uid = u.uid '. $filter['where'] .' ORDER BY m.changed DESC'), 50, 0, NULL, $filter['args']);

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $options = array();
  foreach (module_invoke_all('media_operations') as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'approve',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('mmedia_admin_media_submit'),
  );

  $destination = drupal_get_destination();
  $list = array();
  $user = new stdClass;
  while ($media = db_fetch_object($result)) {
    // define user for theming
    $user->uid = $media->uid;
    $user->name = $media->username;

    $list[$media->mid] = '';

    $form['title'][$media->mid] = array('#value' => l($media->title, MEDIA_PATH .'/'. $media->mid));
    $form['name'][$media->mid] =  array('#value' => theme('username', $user));
    $form['type'][$media->mid] =  array('#value' => $media->type);
    $form['ext'][$media->mid] =  array('#value' => $media->ext);

    $form['operations'][$media->mid] = array('#value' => l(t('edit'), MEDIA_PATH .'/'. $media->mid .'/edit', array('query' => $destination)));
  }
  $form['media'] = array('#type' => 'checkboxes', '#options' => $list);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'mmedia_admin_media';
  return $form;
}

/**
 * Validate mmedia_admin_media form submissions.
 *
 * Check if any media have been selected to perform the chosen
 * 'Update option' on.
 */
function mmedia_admin_media_validate($form, &$form_state) {
  $media = array_filter($form_state['values']['media']);
  if (count($media) == 0) {
    form_set_error('', t('No items selected.'));
  }
}

/**
 * Process mmedia_admin_media form submissions.
 *
 * Execute the chosen 'Update option' on the selected media.
 */
function mmedia_admin_media_submit($form, &$form_state) {
  $operations = module_invoke_all('mmedia_operations');
  $operation = $operations[$form_state['values']['operation']];
  // Filter out unchecked media
  $media = array_filter($form_state['values']['media']);
  if ($function = $operation['callback']) {
    // Add in callback arguments if present.
    if (isset($operation['callback arguments'])) {
      $args = array_merge(array($media), $operation['callback arguments']);
    }
    else {
      $args = array($media);
    }
    call_user_func_array($function, $args);

    cache_clear_all();
  }
  else {
    // We need to rebuild the form to go to a second step.  For example, to
    // show the confirmation form for the deletion of nodes.
    $form_state['rebuild'] = TRUE;
  }
}


/**
 * Theme node administration overview.
 *
 * @ingroup themeable
 */
function theme_mmedia_admin_media($form) {
  // If there are rows in this form, then $form['title'] contains a list of
  // the title form elements.
  $has_posts = isset($form['title']) && is_array($form['title']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Title'), t('Author'), t('Type'), t('Extension'));
  $header[] = t('Operations');
  $output = '';

  $output .= drupal_render($form['options']);
  if ($has_posts) {
    foreach (element_children($form['title']) as $key) {
      $row = array();
      $row[] = drupal_render($form['media'][$key]);
      $row[] = drupal_render($form['title'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['type'][$key]);
      $row[] = drupal_render($form['ext'][$key]);
      $row[] = drupal_render($form['operations'][$key]);
      $rows[] = $row;
    }

  }
  else {
    $rows[] = array(array('data' => t('No posts available.'), 'colspan' => '6'));
  }

  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }

  $output .= drupal_render($form);

  return $output;
}

/**
 *
 */
function mmedia_multiple_delete_confirm(&$form_state, $media) {
  $form['media'] = array('#prefix' => '<ul>', '#suffix' => '</ul>', '#tree' => TRUE);
  // array_filter returns only elements with TRUE values
  foreach ($media as $mid => $value) {
    $title = db_result(db_query('SELECT title FROM {media} WHERE mid = %d', $mid));
    $form['media'][$mid] = array(
      '#type' => 'hidden',
      '#value' => $mid,
      '#prefix' => '<li>',
      '#suffix' => check_plain($title) ."</li>\n",
    );
  }
  $form['operation'] = array('#type' => 'hidden', '#value' => 'delete');
  $form['#submit'][] = 'mmedia_multiple_delete_confirm_submit';
  return confirm_form($form,
                      t('Are you sure you want to delete these items?'),
                      'admin/mapi/media', t('This action cannot be undone.'),
                      t('Delete all'), t('Cancel'));
}

/**
 *
 */
function mmedia_multiple_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    foreach ($form_state['values']['media'] as $mid => $value) {
      media_delete($mid);
    }
    drupal_set_message(t('The items have been deleted.'));
  }
  $form_state['redirect'] = 'admin/mapi/media';
  return;
}

